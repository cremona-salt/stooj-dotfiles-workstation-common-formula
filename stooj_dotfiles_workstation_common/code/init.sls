# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - .directories
  - .repos
  - .utilities
