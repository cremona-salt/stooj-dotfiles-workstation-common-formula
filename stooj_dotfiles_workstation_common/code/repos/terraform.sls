# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set tplsubname = tpldir.split('/')[1] %}
{%- set tplsubroot = tplroot ~ '.' ~ tplsubname %}
{%- set sls_code_directory = tplsubroot ~ '.directories.code_dir' %}

include:
  - {{ sls_code_directory }}

stooj-dotfiles-workstation-common-code-repos-code-terraform-dir:
  file.directory:
    - name: /home/stooj/code/terraform
    - user: stooj
    - group: stooj
    - mode: 700
    - makedirs: True
    - require:
      - sls: {{ sls_code_directory }}

{%- for name, data in stooj_dotfiles_workstation_common.code.terraform_repos.items() %}
stooj-dotfiles-workstation-common-code-repos-code-terraform-{{ name|replace('_', '-') }}-cloned:
  git.latest:
    {%- set origin = data['origin'] %}
    - name: {{ origin }}
    - target: /home/stooj/code/terraform/{{ name }}
    - user: stooj
    {%- if origin.startswith('git') %}
    - identity:
      - salt://ssh/files/deploy_key
    {%- endif %}
    - require:
      - stooj-dotfiles-workstation-common-code-repos-code-terraform-dir

{%- for remote, url in data.items() %}
{%- if remote == 'origin' %}
{# Do nothing - already dealt with origin #}
{%- else %}
stooj-dotfiles-workstation-common-code-repos-terraform-{{ name|replace('_', '-') }}-repo-{{ remote }}-added:
  cmd.run:
    - name: git remote add {{ remote }} {{ url }}
    - cwd: /home/stooj/code/terraform/{{ name }}
    - runas: stooj
    - require:
      - stooj-dotfiles-workstation-common-code-repos-terraform-{{ name|replace('_', '-') }}-cloned
    - unless:
      - grep 'remote "{{ remote }}"' /home/stooj/code/terraform/{{ name }}/.git/config
{%- endif %}
{%- endfor %}

stooj-dotfiles-workstation-common-code-repos-terraform-{{ name|replace('_', '-') }}-group-managed:
  file.directory:
    - name: /home/stooj/code/terraform/{{ name }}
    - group: stooj
    - recurse:
      - group

{%- endfor %}
