# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - .accounts
  - .documentation
  - .misc
  - .salt_repos
  - .terraform
