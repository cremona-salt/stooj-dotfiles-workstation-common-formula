# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - .repos.clean
  - .utilities.clean
  - .directories.clean
