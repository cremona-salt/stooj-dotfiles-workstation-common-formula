# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-pam-environment-config-file-env-config-managed:
  file.managed:
    - name: /home/stooj/.pam_environment
    - source: {{ files_switch(['pam_environment.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-pam-environment-config-file-env-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        pam_environment: {{ stooj_dotfiles_workstation_common.pam_environment | json }}
