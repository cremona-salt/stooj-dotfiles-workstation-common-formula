# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}

stooj-dotfiles-workstation-common-mail-w3m-config-clean-config-absent:
  file.absent:
    - name: /home/stooj/.w3m/config

stooj-dotfiles-workstation-common-mail-w3m-config-clean-dir-absent:
  file.absent:
    - name: /home/stooj/.w3m
