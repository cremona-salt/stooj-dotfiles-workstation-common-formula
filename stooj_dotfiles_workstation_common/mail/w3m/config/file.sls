# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set tplsubname = tpldir.split('/')[1] %}
{%- set tplsubsubname = tpldir.split('/')[2] %}
{%- set tplsubsubroot = tplroot ~ '.' ~ tplsubname ~ '.' ~ tplsubsubname %}
{%- set sls_package_install = tplsubsubroot ~ '.package.install' %}

include:
  - {{ sls_package_install }}

stooj-dotfiles-workstation-common-mail-w3m-config-file-dir-managed:
  file.directory:
    - name: /home/stooj/.w3m
    - user: stooj
    - group: stooj
    - mode: 700
    - makedirs: True

stooj-dotfiles-workstation-common-mail-w3m-config-file-config-managed:
  file.managed:
    - name: /home/stooj/.w3m/config
    - source: {{ files_switch(['w3m/config.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-mail-w3m-config-file-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        w3m: {{ stooj_dotfiles_workstation_common.mail.w3m | json }}
        accounts: {{ stooj_dotfiles_workstation_common.mail.accounts | json }}
    - require:
      - stooj-dotfiles-workstation-common-mail-w3m-config-file-dir-managed
