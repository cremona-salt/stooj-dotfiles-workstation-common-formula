# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}

stooj-dotfiles-workstation-common-mail-w3m-package-install-pkg-installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles_workstation_common.mail.w3m.pkgs | yaml }}
