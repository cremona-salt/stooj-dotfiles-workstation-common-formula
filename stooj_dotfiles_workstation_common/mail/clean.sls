# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - .w3m.clean
  - .isync.clean
  - .notmuch.clean
  - .mutt.clean
  - .msmtp.clean
  - .imapfilter.clean
