# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}

stooj-dotfiles-workstation-common-mail-mutt-config-file-pgp-absent:
  file.absent:
    - name: /home/stooj/.config/mutt/pgp

stooj-dotfiles-workstation-common-mail-mutt-config-file-mailcap-absent:
  file.absent:
    - name: /home/stooj/.config/mutt/mailcap

stooj-dotfiles-workstation-common-mail-mutt-config-file-colours-absent:
  file.absent:
    - name: /home/stooj/.config/mutt/colours

{%- for trigger, data in stooj_dotfiles_workstation_common.mail.mutt.triggers.items() %}
stooj-dotfiles-workstation-common-mail-mutt-config-file-trigger-{{ trigger }}-absent:
  file.absent:
    - name: /home/stooj/.config/mutt/{{ trigger }}
{%- endfor %}

stooj-dotfiles-workstation-common-mail-mutt-config-file-macros-absent:
  file.absent:
    - name: /home/stooj/.config/mutt/macros

stooj-dotfiles-workstation-common-mail-mutt-config-file-common-absent:
  file.absent:
    - name: /home/stooj/.config/mutt/common

{%- for account, data in stooj_dotfiles_workstation_common.mail.accounts.items() %}
stooj-dotfiles-workstation-common-mail-mutt-config-file-accounts-{{ account }}-absent:
  file.absent:
    - name: /home/stooj/.config/mutt/{{ account }}
{%- endfor %}

stooj-dotfiles-workstation-common-mail-mutt-config-file-config-absent:
  file.absent:
    - name: /home/stooj/.config/mutt/muttrc

stooj-dotfiles-workstation-common-mail-mutt-config-file-zsh-alias-absent:
  file.absent:
    - name: /home/stooj/.config/zsh/rc_includes/neomutt-alias.zsh
