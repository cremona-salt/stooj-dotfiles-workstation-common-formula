# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-mail-mutt-config-file-config-managed:
  file.managed:
    - name: /home/stooj/.config/mutt/muttrc
    - source: {{ files_switch(['mutt/muttrc.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-mail-mutt-config-file-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        mutt: {{ stooj_dotfiles_workstation_common.mail.mutt | json }}
        accounts: {{ stooj_dotfiles_workstation_common.mail.accounts | json }}

{%- for account, data in stooj_dotfiles_workstation_common.mail.accounts.items() %}
stooj-dotfiles-workstation-common-mail-mutt-config-file-accounts-{{ account }}-managed:
  file.managed:
    - name: /home/stooj/.config/mutt/account.{{ account }}
    - source: {{ files_switch(['mutt/accounts.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-mail-mutt-config-file-accounts-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        config : {{ data | json }}
        account: {{ account | json }}

{%- endfor %}

{%- for trigger, data in stooj_dotfiles_workstation_common.mail.mutt.triggers.items() %}
stooj-dotfiles-workstation-common-mail-mutt-config-file-trigger-{{ trigger }}-managed:
  file.managed:
    - name: /home/stooj/.config/mutt/{{ trigger }}
    - source: {{ files_switch(['mutt/trigger.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-mail-mutt-config-file-trigger-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        account: {{ data.account }}
        address: {{ data.address }}
{%- endfor %}

{%- for conffile in [
  'binds',
  'colours',
  'colours-extended',
  'common',
  'composing_options',
  'directories',
  'editor',
  'formats',
  'headers',
  'hooks',
  'index_options',
  'mailcap',
  'macros',
  'pager_options'
  'pgp'] %}

stooj-dotfiles-workstation-common-mail-mutt-config-file-{{ conffile|replace('_', '-') }}-managed:
  file.managed:
    - name: /home/stooj/.config/mutt/{{ conffile }}
    - source: {{ files_switch(['mutt/' ~ conffile ~ '.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-mail-mutt-config-file-' ~ conffile|replace('_', '-') ~ '-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        mutt: {{ stooj_dotfiles_workstation_common.mail.mutt | json }}
        accounts: {{ stooj_dotfiles_workstation_common.mail.accounts | json }}
{%- endfor %}

stooj-dotfiles-workstation-common-mail-mutt-config-file-zsh-alias-managed:
  file.managed:
    - name: /home/stooj/.config/zsh/rc_includes/neomutt-alias.zsh
    - source: {{ files_switch(['mutt/neomutt-alias.zsh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-mail-mutt-config-file-zsh-alias-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        mutt: {{ stooj_dotfiles_workstation_common.mail.mutt | json }}

stooj-dotfiles-workstation-common-mail-mutt-config-file-send-from-mutt-managed:
  file.managed:
    - name: /home/stooj/bin/send-from-mutt
    - source: {{ files_switch(['mutt/send-from-mutt.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-mail-mutt-config-file-send-from-mutt-managed',
                              use_subpath=True
                             )
              }}
    - mode: 700
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        mutt: {{ stooj_dotfiles_workstation_common.mail.mutt | json }}
        accounts: {{ stooj_dotfiles_workstation_common.mail.accounts | json }}

stooj-dotfiles-workstation-common-mail-mutt-config-file-convert-to-html-multipart-managed:
  file.managed:
    - name: /home/stooj/bin/convert-to-html-multipart
    - source: {{ files_switch(['mutt/convert-to-html-multipart.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-mail-mutt-config-file-convert-to-html-multipart-managed',
                              use_subpath=True
                             )
              }}
    - mode: 700
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        mutt: {{ stooj_dotfiles_workstation_common.mail.mutt | json }}
