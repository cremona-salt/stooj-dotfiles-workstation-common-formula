# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- for account, data in stooj_dotfiles_workstation_common.mail.accounts.items() %}
stooj-dotfiles-workstation-common-mail-isync-config-file-{{ account }}-sync-managed:
  file.managed:
    - name: /home/stooj/.config/isync/account_{{ account }}.mbsyncrc
    - source: {{ files_switch(['isync/config.mbsyncrc.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-mail-isync-config-file-account-sync-managed',
                              use_subpath=True
                              )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        account: {{ account | json }}
        data: {{ data | json }}

stooj-dotfiles-workstation-common-mail-isync-config-lib-dir-managed:
  file.directory:
    - name: /home/stooj/.local/share/mail/{{ account }}
    - mode: 700
    - user: stooj
    - group: stooj
    - makedirs: True

{%- endfor -%}

stooj-dotfiles-workstation-common-mail-isync-config-lib-dir-managed:
  file.directory:
    - name: /home/stooj/.config/mail/lib
    - mode: 700
    - user: stooj
    - group: stooj
    - makedirs: True

stooj-dotfiles-workstation-common-mail-isync-config-lib-fetch-mail-script-managed:
  file.managed:
    - name: /home/stooj/.config/mail/lib/fetch-mail
    - source: {{ files_switch(['isync/fetch-mail.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-mail-isync-config-lib-fetch-mail-script-managed',
                              use_subpath=True
                             )
              }}
    - mode: 700
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        isync: {{ stooj_dotfiles_workstation_common.mail.isync | json }}
    - require:
      - stooj-dotfiles-workstation-common-mail-isync-config-lib-dir-managed

stooj-dotfiles-workstation-common-mail-isync-config-fetch-mail-service-script-managed:
  file.managed:
    - name: /home/stooj/.config/systemd/user/fetch-mail.service
    - source: {{ files_switch(['isync/fetch-mail.service.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-mail-isync-config-fetch-mail-service-script-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        isync: {{ stooj_dotfiles_workstation_common.mail.isync | json }}

stooj-dotfiles-workstation-common-mail-isync-config-fetch-mail-timer-script-managed:
  file.managed:
    - name: /home/stooj/.config/systemd/user/fetch-mail.timer
    - source: {{ files_switch(['isync/fetch-mail.timer.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-mail-isync-config-fetch-mail-timer-script-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        isync: {{ stooj_dotfiles_workstation_common.mail.isync | json }}

stooj-dotfiles-workstation-common-mail-isync-config-start-mail-services-script-created:
  file.managed:
    - name: /home/stooj/.config/i3/autostart.d/fetch-mail
    - user: stooj
    - group: stooj
    - mode: 700
    - makedirs: True
    - replace: False

stooj-dotfiles-workstation-common-mail-isync-config-start-mail-services-script-managed:
  file.prepend:
    - name: /home/stooj/.config/i3/autostart.d/fetch-mail
    - text:
      - "#!/bin/bash"
      - "#############################################################################"
      - "#  File managed by Salt at <stooj_dotfiles_workstation_common.mail.isync.config.file>."
      - "#  Your changes will be overwritten."
      - "#############################################################################"
    - makedirs: True
    - require:
      - stooj-dotfiles-workstation-common-mail-isync-config-start-mail-services-script-created

stooj-dotfiles-workstation-common-mail-isync-config-start-mail-services-script-pullmail-timer-added:
  file.append:
    - name: /home/stooj/.config/i3/autostart.d/fetch-mail
    - text:
      - systemctl --user start bridge.service
      - systemctl --user start fetch-mail.timer
    - makedirs: True
    - require:
      - stooj-dotfiles-workstation-common-mail-isync-config-start-mail-services-script-created
