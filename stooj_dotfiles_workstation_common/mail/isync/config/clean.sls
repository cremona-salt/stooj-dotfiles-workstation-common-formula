# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}

stooj-dotfiles-workstation-common-mail-isync-config-start-mail-services-script-absent:
  file.absent:
    - name: /home/stooj/.config/i3/autostart.d/mail-services

stooj-dotfiles-workstation-common-mail-isync-config-pullmail-timer-script-absent:
  file.absent:
    - name: /home/stooj/.config/systemd/user/pullmail.timer

stooj-dotfiles-workstation-common-mail-isync-config-pullmail-service-script-absent:
  file.absent:
    - name: /home/stooj/.config/systemd/user/pullmail.service

stooj-dotfiles-workstation-common-mail-isync-config-lib-isync-get-passwords-script-absent:
  file.absent:
    - name: /home/stooj/.local/share/isync/lib/isync-get-passwords.py

stooj-dotfiles-workstation-common-mail-isync-config-lib-pull-mail-script-absent:
  file.absent:
    - name: /home/stooj/.local/share/isync/lib/pull-mail

stooj-dotfiles-workstation-common-mail-isync-config-lib-dir-absent:
  file.directory:
    - name: /home/stooj/.local/share/isync/lib

stooj-dotfiles-workstation-common-mail-isync-config-clean-config-absent:
  file.absent:
    - name: /home/stooj/.isyncrc
