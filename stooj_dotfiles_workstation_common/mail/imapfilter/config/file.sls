# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- for account, data in stooj_dotfiles_workstation_common.mail.accounts.items() %}
{%- if 'filters' in data %}
stooj-dotfiles-workstation-common-mail-imapfilter-config-file-imapfilter-{{ account }}-managed:
  file.managed:
    - name: /home/stooj/.config/imapfilter/{{ account }}.lua
    - source: {{ files_switch(['imapfilter/imapfilter.lua.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-mail-imapfilter-config-file-imapfilter-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        account: {{ account | json }}
        data : {{ data | json }}
{%- endif %}
{%- endfor %}
