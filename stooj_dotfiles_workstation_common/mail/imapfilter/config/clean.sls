# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}

stooj-dotfiles-workstation-common-mail-imapfilter-config-file-pgp-absent:
  file.absent:
    - name: /home/stooj/.config/imapfilter/pgp

stooj-dotfiles-workstation-common-mail-imapfilter-config-file-mailcap-absent:
  file.absent:
    - name: /home/stooj/.config/imapfilter/mailcap

stooj-dotfiles-workstation-common-mail-imapfilter-config-file-colours-absent:
  file.absent:
    - name: /home/stooj/.config/imapfilter/colours

{%- for trigger, data in stooj_dotfiles_workstation_common.mail.imapfilter.triggers.items() %}
stooj-dotfiles-workstation-common-mail-imapfilter-config-file-trigger-{{ trigger }}-absent:
  file.absent:
    - name: /home/stooj/.config/imapfilter/{{ trigger }}
{%- endfor %}

stooj-dotfiles-workstation-common-mail-imapfilter-config-file-macros-absent:
  file.absent:
    - name: /home/stooj/.config/imapfilter/macros

stooj-dotfiles-workstation-common-mail-imapfilter-config-file-common-absent:
  file.absent:
    - name: /home/stooj/.config/imapfilter/common

{%- for account, data in stooj_dotfiles_workstation_common.mail.accounts.items() %}
stooj-dotfiles-workstation-common-mail-imapfilter-config-file-accounts-{{ account }}-absent:
  file.absent:
    - name: /home/stooj/.config/imapfilter/{{ account }}
{%- endfor %}

stooj-dotfiles-workstation-common-mail-imapfilter-config-file-config-absent:
  file.absent:
    - name: /home/stooj/.config/imapfilter/imapfilterrc
