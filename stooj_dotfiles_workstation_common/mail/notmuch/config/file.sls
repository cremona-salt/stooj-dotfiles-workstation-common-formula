# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set tplsubname = tpldir.split('/')[1] %}
{%- set tplsubsubname = tpldir.split('/')[2] %}
{%- set tplsubroot = tplroot ~ '.' ~ tplsubname %}
{%- set tplsubsubroot = tplroot ~ '.' ~ tplsubname ~ '.' ~ tplsubsubname %}
{%- set sls_package_install = tplsubsubroot ~ '.package.install' %}
{%- set sls_mutt_config_file = tplsubroot ~ '.mutt.config.file' %}

include:
  - {{ sls_mutt_config_file }}


stooj-dotfiles-workstation-common-mail-notmuch-config-file-config-managed:
  file.managed:
    - name: /home/stooj/.config/notmuch/config
    - source: {{ files_switch(['notmuch/notmuch-config.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-mail-notmuch-config-file-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        notmuch: {{ stooj_dotfiles_workstation_common.mail.notmuch | json }}
        accounts: {{ stooj_dotfiles_workstation_common.mail.accounts | json }}

stooj-dotfiles-workstation-common-mail-notmuch-config-file-env-includes-file-managed:
  file.managed:
    - name: /home/stooj/.config/zsh/env_includes/notmuch.zsh
    - source: {{ files_switch(['notmuch/notmuch.zsh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-mail-notmuch-config-file-env-includes-file-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        notmuch: {{ stooj_dotfiles_workstation_common.mail.notmuch | json }}
