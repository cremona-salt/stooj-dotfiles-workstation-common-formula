Mail TODOs
==========

- Update paths
  - Configs in ~/.config/mail_config or app-specific dirs
  - Scripts in ~/.local/share/ ...

- Replace lbdb with khard on home machines and goobook on work laptop.
  Goobook is only in the aur, so install manually for now.lj
- Replace mairix with notmuch

- Install imapfilter somehow
