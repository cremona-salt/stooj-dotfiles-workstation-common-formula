# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set tplsubname = tpldir.split('/')[1] %}
{%- set tplsubsubname = tpldir.split('/')[2] %}
{%- set tplsubsubroot = tplroot ~ '.' ~ tplsubname ~ '.' ~ tplsubsubname %}
{%- set sls_package_install = tplsubsubroot ~ '.package.install' %}

include:
  - {{ sls_package_install }}

stooj-dotfiles-workstation-common-mail-msmtp-config-file-dir-managed:
  file.directory:
    - name: /home/stooj/.config/msmtp
    - user: stooj
    - group: stooj
    - mode: 700
    - makedirs: True

stooj-dotfiles-workstation-common-mail-msmtp-config-file-config-managed:
  file.managed:
    - name: /home/stooj/.config/msmtp/config
    - source: {{ files_switch(['msmtp/config.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-mail-msmtp-config-file-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        msmtp: {{ stooj_dotfiles_workstation_common.mail.msmtp | json }}
        accounts: {{ stooj_dotfiles_workstation_common.mail.accounts | json }}
    - require:
      - stooj-dotfiles-workstation-common-mail-msmtp-config-file-dir-managed

stooj-dotfiles-workstation-common-mail-msmtp-config-file-log-dir-managed:
  file.directory:
    - name: /home/stooj/.cache/msmtp
    - user: stooj
    - group: stooj
    - mode: 700
    - makedirs: True

stooj-dotfiles-workstation-common-mail-msmtp-config-file-log-file-managed:
  file.managed:
    - name: /home/stooj/.caceh/msmtp/msmtp.log
    - source: ~
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - replace: False
    - require:
      - stooj-dotfiles-workstation-common-mail-msmtp-config-file-log-dir-managed
