# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - .imapfilter
  - .msmtp
  - .mutt
  - .notmuch
  - .isync
  - .w3m
