# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-konsole-config-file-env-includes-config-absent:
  file.absent:
    - name: /home/stooj/.config/zsh/env_includes/konsole.zsh

stooj-dotfiles-workstation-common-konsole-config-file-konsolerc-absent:
  file.absent:
    - name: /home/stooj/.config/konsolerc

stooj-dotfiles-workstation-common-konsole-config-file-stoos-profile-absent:
  file.managed:
    - name: /home/stooj/.local/share/konsole/stoos-profile

stooj-dotfiles-workstation-common-konsole-config-file-gruvbox-dark-theme-absent:
  file.absent:
    - name: /home/stooj/.local/share/konsole/Gruvbox_dark.colorscheme

stooj-dotfiles-workstation-common-konsole-config-file-local-dir:
  file.absent:
    - name: /home/stooj/.local/share/konsole
