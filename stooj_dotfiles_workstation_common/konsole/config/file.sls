# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}


stooj-dotfiles-workstation-common-konsole-config-file-konsolerc-managed:
  file.managed:
    - name: /home/stooj/.config/konsolerc
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - replace: False

stooj-dotfiles-workstation-common-konsole-config-file-konsolerc-content-managed:
  ini.options_present:
    - name: /home/stooj/.config/konsolerc
    - sections:
        'Desktop Entry':
          DefaultProfile: stoos-profile.profile
        'Favorite Profiles':
          Favorites: stoos-profile.profile
        KonsoleWindow:
          ShowMenuBarByDefault: False
        TabBar:
          TabBarPosition: Top
          TabBarVisibility: ShowTabBarWhenNeeded
    - require:
      - stooj-dotfiles-workstation-common-konsole-config-file-konsolerc-managed

stooj-dotfiles-workstation-common-konsole-config-file-stoos-profile-managed:
  file.managed:
    - name: /home/stooj/.local/share/konsole/stoos-profile.profile
    - source: {{ files_switch(['stoos-profile.profile.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-konsole-config-file-stoos-profile-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        konsole: {{ stooj_dotfiles_workstation_common.konsole | json }}

stooj-dotfiles-workstation-common-konsole-config-file-gruvbox-dark-theme-managed:
  file.managed:
    - name: /home/stooj/.local/share/konsole/Gruvbox_dark.colorscheme
    - source: {{ files_switch(['Gruvbox_dark.colorscheme.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-konsole-config-file-gruvbox-dark-theme-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        konsole: {{ stooj_dotfiles_workstation_common.konsole | json }}

stooj-dotfiles-workstation-common-konsole-config-file-env-includes-config-managed:
  file.managed:
    - name: /home/stooj/.config/zsh/env_includes/konsole.zsh
    - source: {{ files_switch(['konsole.zsh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-konsole-config-file-env-includes-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        konsole: {{ stooj_dotfiles_workstation_common.konsole | json }}
