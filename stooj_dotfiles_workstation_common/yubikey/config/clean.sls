# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- for key in stooj_dotfiles_workstation_common.get('yubikey:trusted_keys', []) %}
stooj-dotfiles-workstation-common-yubikey-config-key-{{ key }}-absent:
  gpg.absent:
    - name: {{ key }}
    - keys: {{ key }}
    - user: stooj
{%- endfor %}

stooj-dotfiles-workstation-common-yubikey-config-file-udev-gnupg-ccid-rules-absent:
  file.absent:
    - name: /etc/udev/rules.d/71-gnupg-ccid.rules
