# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-about-config-repo-rc-file-absent:
  file.absent:
    - name: /home/stooj/.config/zsh/rc_includes/about.zsh

{%- set repo = stooj_dotfiles_workstation_common.about.repo %}
{%- set target = stooj_dotfiles_workstation_common.about.target %}

stooj-dotfiles-workstation-common-about-config-repo-about-absent:
  file.absent:
    - name: {{ target }}

stooj-dotfiles-workstation-common-about-config-repo-{{ store }}-alias-absent:
  file.absent:
    - name: /home/stooj/.config/zsh/rc_includes/about.zsh
