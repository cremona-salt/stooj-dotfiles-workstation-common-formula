# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set sls_sphinx_installed = tplroot ~ '.sphinx.package.install' %}

include:
  - {{ sls_sphinx_installed }}

stooj-dotfiles-workstation-common-about-config-repo-config-dir-managed:
  file.directory:
    - name: /home/stooj/code/docs
    - user: stooj
    - group: stooj
    - mode: 700
    - makedirs: True

stooj-dotfiles-workstation-common-about-config-repo-rc-file-managed:
  file.managed:
    - name: /home/stooj/.config/zsh/rc_includes/about.zsh
    - source: {{ files_switch(['about.zsh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-about-config-repo-rc-file-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        about: {{ stooj_dotfiles_workstation_common.about | yaml }}

{%- set repo = stooj_dotfiles_workstation_common.about.repo %}
{%- set target = stooj_dotfiles_workstation_common.about.target %}
stooj-dotfiles-workstation-common-about-config-repo-about-cloned:
  git.latest:
    - name: {{ repo }}
    - target: {{ target }}
    - user: stooj
    {%- if repo != 'https://github.com/saltstack-formulas/template-formula.git' %}
    {# This is to allow tests to about without access to a real ssh key #}
    - identity:
      - salt://ssh/files/deploy_key
    {%- endif %}

stooj-dotfiles-workstation-common-about-config-repo-about-group-managed:
  file.directory:
    - name: {{ target }}
    - group: stooj
    - recurse:
      - group

stooj-dotfiles-workstation-common-about-config-repo-about-make-html:
  cmd.run:
    - name: make html
    - cwd: {{ target }}
    - runas: stooj
    - onchanges:
      - stooj-dotfiles-workstation-common-about-config-repo-about-cloned
    - require:
      - sls: {{ sls_sphinx_installed }}
      - stooj-dotfiles-workstation-common-about-config-repo-about-cloned
