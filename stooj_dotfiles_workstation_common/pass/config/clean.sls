# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- for store, data in stooj_dotfiles_workstation_common.pass.get('pass_stores', {}).items() %}
stooj-dotfiles-workstation-common-pass-config-repo-{{ store }}-pass-absent:
  file.absent:
    - name: {{ data.target }}

{%- if data.alias is defined %}
stooj-dotfiles-workstation-common-pass-config-repo-{{ store }}-alias-absent:
  file.absent:
    - name: /home/stooj/.config/zsh/rc_includes/{{ store }}-pass-alias.zsh
{%- endif %}
{%- endfor %}
