# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- for store, data in stooj_dotfiles_workstation_common.pass.get('pass_stores', {}).items() %}
stooj-dotfiles-workstation-common-pass-config-repo-{{ store }}-pass-cloned:
  git.latest:
    - name: {{ data.repo }}
    - target: {{ data.target }}
    - user: stooj
    {%- if data.repo != 'https://github.com/saltstack-formulas/template-formula.git' %}
    {# This is to allow tests to pass without access to a real ssh key #}
    - identity:
      - salt://ssh/files/deploy_key
    {%- endif %}

stooj-dotfiles-workstation-common-pass-config-repo-{{ store }}-pass-group-managed:
  file.directory:
    - name: {{ data.target }}
    - user: stooj
    - group: stooj
    - recurse:
      - user
      - group

{%- if data.alias is defined %}
stooj-dotfiles-workstation-common-pass-config-repo-{{ store }}-alias-managed:
  file.managed:
    - name: /home/stooj/.config/zsh/rc_includes/{{ store }}-pass-alias.zsh
    - source: {{ files_switch(['pass-alias.zsh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-pass-config-repo-' ~ store ~ '-alias-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        alias: {{ data.alias }}
        target: {{ data.target }}
{%- endif %}
{%- endfor %}
