# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set sls_cron_present = tplroot ~ '.cron.package.install' %}

include:
  - {{ sls_cron_present }}

stooj-dotfiles-workstation-common-check-battery-config-file-bin-script-managed:
  file.managed:
    - name: /home/stooj/bin/check-battery
    - source: {{ files_switch(['check-battery.sh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-check-battery-config-file-bin-script-managed',
                              use_subpath=True
                             )
              }}
    - mode: 700
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        check_battery: {{ stooj_dotfiles_workstation_common.check_battery | json }}

{#
stooj-dotfiles-workstation-common-check-battery-config-file-cron-entry-created:
  cron.present:
    - name: "$HOME"/bin/check-battery
    - user: stooj
    - minute: 5
    - require:
      - sls: {{ sls_cron_present }}
      #}
