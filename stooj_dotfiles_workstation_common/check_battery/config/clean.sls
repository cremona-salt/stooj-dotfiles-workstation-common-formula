# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-check-battery-config-file-bin-script-absent:
  file.absent:
    - name: /home/stooj/bin/check-battery

stooj-dotfiles-workstation-common-check-battery-config-file-cron-entry-absent:
  cron.absent:
    - name: "$HOME"/bin/check-battery
    - user: stooj
