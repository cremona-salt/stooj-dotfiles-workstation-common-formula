# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-gpg-config-file-gnupg-config-absent:
  file.absent:
    - name: /home/stooj/.gnupg/gpg.conf

stooj-dotfiles-workstation-common-gpg-config-file-gnupg-agent-absent:
  file.absent:
    - name: /home/stooj/.gnupg/gpg-agent.conf

stooj-dotfiles-workstation-common-gpg-config-file-gnupg-dir-absent:
  file.absent:
    - name: /home/stooj/.gnupg

