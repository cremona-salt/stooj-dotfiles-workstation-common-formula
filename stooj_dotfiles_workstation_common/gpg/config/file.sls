# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}


stooj-dotfiles-workstation-common-gpg-config-file-gnupg-dir-managed:
  file.directory:
    - name: /home/stooj/.gnupg
    - user: stooj
    - group: stooj
    - mode: 700
    - makedirs: True

stooj-dotfiles-workstation-common-gpg-config-file-gnupg-config-managed:
  file.managed:
    - name: /home/stooj/.gnupg/gpg.conf
    - source: {{ files_switch(['gpg.conf.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-gpg-config-file-gnupg-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        gpg: {{ stooj_dotfiles_workstation_common.gpg | json }}
    - require:
      - stooj-dotfiles-workstation-common-gpg-config-file-gnupg-dir-managed

stooj-dotfiles-workstation-common-gpg-config-file-gnupg-agent-managed:
  file.managed:
    - name: /home/stooj/.gnupg/gpg-agent.conf
    - source: {{ files_switch(['gpg-agent.conf.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-gpg-config-file-gnupg-agent-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        gpg: {{ stooj_dotfiles_workstation_common.gpg | json }}
    - require:
      - stooj-dotfiles-workstation-common-gpg-config-file-gnupg-dir-managed
