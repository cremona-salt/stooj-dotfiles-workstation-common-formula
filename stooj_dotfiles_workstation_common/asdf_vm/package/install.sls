# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- set sls_local_share_present = 'stooj_dotfiles_common.directories.local_share.directory' %}

include:
  - {{ sls_local_share_present }}

stooj-dotfiles-workstation-common-asdf-vm-package-install-repo-cloned:
  git.latest:
    - name: https://github.com/asdf-vm/asdf.git
    - rev: v0.7.8
    - target: /home/stooj/.local/share/asdf
    - user: stooj
    - require:
      - sls: {{ sls_local_share_present }}

stooj-dotfiles-workstation-common-asdf-vm-package-install-repo-group-ownership-managed:
  file.directory:
    - name: /home/stooj/.local/share/asdf
    - group: stooj
    - recurse:
      - group
    - require:
      - stooj-dotfiles-workstation-common-asdf-vm-package-install-repo-cloned
