# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-asdf-vm-config-file-env-includes-zsh-absent:
  file.absent:
    - name: /home/stooj/.config/zsh/env_includes/asdf_vm.zsh

stooj-dotfiles-workstation-common-asdf-vm-config-file-rc-includes-zsh-absent:
  file.absent:
    - name: /home/stooj/.config/zsh/rc_includes/asdf_vm.zsh

stooj-dotfiles-workstation-common-asdf-vm-config-file-base-tool-versions-absent:
  file.absent:
    - name: /home/stooj/.tool-versions
