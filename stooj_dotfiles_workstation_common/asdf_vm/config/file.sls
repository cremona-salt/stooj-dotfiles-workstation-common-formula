# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-asdf-vm-config-file-env-includes-zsh-managed:
  file.managed:
    - name: /home/stooj/.config/zsh/env_includes/asdf_vm.zsh
    - source: {{ files_switch(['asdf_vm_env_vars.zsh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-asdf-vm-config-file-env-includes-zsh-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        asdf_vm: {{ stooj_dotfiles_workstation_common.asdf_vm | json }}

stooj-dotfiles-workstation-common-asdf-vm-config-file-rc-includes-zsh-managed:
  file.managed:
    - name: /home/stooj/.config/zsh/rc_includes/asdf_vm.zsh
    - source: {{ files_switch(['asdf_vm_rc.zsh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-asdf-vm-config-file-rc-includes-zsh-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        asdf_vm: {{ stooj_dotfiles_workstation_common.asdf_vm | json }}

stooj-dotfiles-workstation-common-asdf-vm-config-file-base-tool-versions-managed:
  file.managed:
    - name: /home/stooj/.tool-versions
    - source: {{ files_switch(['top-tool-versions.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-asdf-vm-config-file-base-tool-versions-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        asdf_vm: {{ stooj_dotfiles_workstation_common.asdf_vm | json }}
