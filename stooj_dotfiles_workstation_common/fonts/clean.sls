# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - .nerdfonts.clean
  - .custom.clean
  - .package.clean
