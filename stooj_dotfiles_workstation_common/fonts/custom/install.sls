# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}

stooj-dotfiles-workstation-common-fonts-custom-install-font-dir-managed:
  file.directory:
    - name: /usr/local/share/fonts
    - user: root
    - group: root
    - mode: 755
    - makedirs: False

stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-dir-managed:
  file.directory:
    - name: /usr/local/share/fonts/Powerlevel10k
    - user: root
    - group: root
    - mode: 755
    - makedirs: False
    - require:
      - stooj-dotfiles-workstation-common-fonts-custom-install-font-dir-managed

stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-regular-font-managed:
  file.managed:
    - name: /usr/local/share/fonts/Powerlevel10k/MesloLGS NF Regular.ttf
    - source: https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf
    - source_hash: 9192e07bc2e8aa973931b86f2ac6946d727319b07f1fdf1a90756b0da9accdcb
    - require:
      - stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-dir-managed

stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-bold-italic-font-managed:
  file.managed:
    - name: /usr/local/share/fonts/Powerlevel10k/MesloLGS NF Bold Italic.ttf
    - source: https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf
    - source_hash: 7fed7e876c1a911ebdd83cb861d4d26fc81376da2018ac8d704a99897c35b83c
    - require:
      - stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-dir-managed

stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-bold-font-managed:
  file.managed:
    - name: /usr/local/share/fonts/Powerlevel10k/MesloLGS NF Bold.ttf
    - source: https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf
    - source_hash: ebdc86f4dbac57ec81e3b7b6f2e6fa182b4ac74e928f2035d398000404f42b71
    - require:
      - stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-dir-managed

stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-italic-font-managed:
  file.managed:
    - name: /usr/local/share/fonts/Powerlevel10k/MesloLGS NF Italic.ttf
    - source: https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf
    - source_hash: ffbb03ec5ae9be0bed5609c423f1846c59d94641c4f6a2ddf94e24f8c7958290
    - require:
      - stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-dir-managed

stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-update-cache:
  cmd.run:
    - name: fc-cache --force
    - watch:
      - stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-regular-font-managed
      - stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-bold-italic-font-managed
      - stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-bold-font-managed
      - stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-italic-font-managed
