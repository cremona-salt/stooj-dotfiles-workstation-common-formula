# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}

stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-regular-font-absent:
  file.absent:
    - name: /usr/local/share/fonts/Powerlevel10k/MesloLGS NF Regular.ttf

stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-bold-italic-font-absent:
  file.absent:
    - name: /usr/local/share/fonts/Powerlevel10k/MesloLGS NF Bold Italic.ttf

stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-bold-font-absent:
  file.absent:
    - name: /usr/local/share/fonts/Powerlevel10k/MesloLGS NF Bold.ttf

stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-italic-font-absent:
  file.absent:
    - name: /usr/local/share/fonts/Powerlevel10k/MesloLGS NF Italic.ttf

stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-dir-managed:
  file.directory:
    - name: /usr/local/share/fonts/Powerlevel10k

stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-update-cache:
  cmd.run:
    - name: fc-cache --force
    - watch:
      - stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-regular-font-absent
      - stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-bold-italic-font-absent
      - stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-bold-font-absent
      - stooj-dotfiles-workstation-common-fonts-custom-install-powerlevel-font-italic-font-absent

