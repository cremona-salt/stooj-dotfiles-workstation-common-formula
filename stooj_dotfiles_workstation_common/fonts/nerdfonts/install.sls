# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}

stooj-dotfiles-workstation-common-fonts-nerdfonts-repo-cloned:
  git.latest:
    - name: https://github.com/ryanoasis/nerd-fonts.git
    - target: /srv/nerdfonts
    - depth: 1
    - rev: 2.1.0
    - unless: ls /usr/local/share/fonts/NerdFonts

stooj-dotfiles-workstation-common-fonts-nerdfonts-package-installed:
  cmd.run:
    - name: /srv/nerdfonts/install.sh --install-to-system-path
    - require:
      - stooj-dotfiles-workstation-common-fonts-nerdfonts-repo-cloned
    - creates: /usr/local/share/fonts/NerdFonts

stooj-dotfiles-workstation-common-fonts-nerdfonts-repo-removed:
  file.absent:
    - name: /srv/nerdfonts
    - require:
      - stooj-dotfiles-workstation-common-fonts-nerdfonts-package-installed
