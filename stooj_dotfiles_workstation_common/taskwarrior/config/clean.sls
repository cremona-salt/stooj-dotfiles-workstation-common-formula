# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- for store, data in stooj_dotfiles_workstation_common.taskwarrior.get('taskwarrior_stores', {}).items() %}
stooj-dotfiles-workstation-common-taskwarrior-config-repo-{{ store }}-taskwarrior-absent:
  file.absent:
    - name: {{ data.target }}

{%- if data.alias is defined %}
stooj-dotfiles-workstation-common-taskwarrior-config-repo-{{ store }}-alias-absent:
  file.absent:
    - name: /home/stooj/.config/zsh/rc_includes/{{ store }}-taskwarrior-alias.zsh
{%- endif %}
{%- endfor %}
