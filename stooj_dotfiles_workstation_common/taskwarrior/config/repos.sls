# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-taskwarrior-config-repo-config-dir-managed:
  file.directory:
    - name: /home/stooj/.config/task
    - user: stooj
    - group: stooj
    - mode: 700
    - makedirs: True

stooj-dotfiles-workstation-common-taskwarrior-config-repo-env-file-managed:
  file.managed:
    - name: /home/stooj/.config/zsh/env_includes/taskwarrior.zsh
    - source: {{ files_switch(['taskwarrior.zsh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-taskwarrior-config-repo-env-file-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        taskwarrior: {{ stooj_dotfiles_workstation_common.taskwarrior | yaml }}

{%- for store, data in stooj_dotfiles_workstation_common.taskwarrior.get('taskwarrior_stores', {}).items() %}
stooj-dotfiles-workstation-common-taskwarrior-config-repo-{{ store }}-taskwarrior-cloned:
  git.latest:
    - name: {{ data.repo }}
    - target: {{ data.target }}
    - user: stooj
    {%- if data.repo != 'https://github.com/saltstack-formulas/template-formula.git' %}
    {# This is to allow tests to taskwarrior without access to a real ssh key #}
    - identity:
      - salt://ssh/files/deploy_key
    {%- endif %}

stooj-dotfiles-workstation-common-taskwarrior-config-repo-{{ store }}-taskwarrior-group-managed:
  file.directory:
    - name: {{ data.target }}
    - group: stooj
    - recurse:
      - group

{%- if data.alias %}
{%- set conffile = '/home/stooj/config/task/' ~ alias ~ '-taskrc' %}
{%- else %}
{%- set conffile = '/home/stooj/.config/task/taskrc' %}
{%- endif %}
stooj-dotfiles-workstation-common-taskwarrior-config-repo-{{ store }}-config-managed:
  file.managed:
    - name: {{ conffile }}
    - source: {{ files_switch(['task-config.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-taskwarrior-config-repo-' ~ store ~ '-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        target: {{ data.target }}

{%- if data.alias %}
stooj-dotfiles-workstation-common-taskwarrior-config-repo-{{ store }}-alias-managed:
  file.managed:
    - name: /home/stooj/.config/zsh/rc_includes/{{ store }}-taskwarrior-alias.zsh
    - source: {{ files_switch(['taskwarrior-alias.zsh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-taskwarrior-config-repo-' ~ store ~ '-alias-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        alias: {{ data.alias }}
{%- else %}
{%- endif %}
{%- endfor %}
