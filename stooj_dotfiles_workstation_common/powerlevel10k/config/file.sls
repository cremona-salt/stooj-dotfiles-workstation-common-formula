# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-powerlevel10k-config-file-p10k-managed:
  file.managed:
    - name: /home/stooj/.config/powerlevel10k/powerlevel10k.zsh
    - source: {{ files_switch(['powerlevel10k.zsh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-powerlevel10k-config-file-p10k-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        powerlevel10k: {{ stooj_dotfiles_workstation_common.powerlevel10k | json }}
