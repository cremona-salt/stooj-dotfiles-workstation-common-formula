# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-xdg-config-file-xdg-user-dirs-conf-absent:
  file.absent:
    - name: /home/stooj/.config/user-dirs.dirs

stooj-dotfiles-workstation-common-xdg-config-file-xdg-user-dirs-locale-conf-absent:
  file.absent:
    - name: /home/stooj/.config/user-dirs.locale
