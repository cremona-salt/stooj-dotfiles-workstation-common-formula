# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-xdg-config-file-xdg-user-dirs-conf-managed:
  file.managed:
    - name: /home/stooj/.config/user-dirs.dirs
    - source: {{ files_switch(['user-dirs.dirs.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-xdg-config-file-xdg-user-dirs-conf-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - context:
        xdg: {{ stooj_dotfiles_workstation_common.xdg | json }}

stooj-dotfiles-workstation-common-xdg-config-file-xdg-user-dirs-locale-conf-managed:
  file.managed:
    - name: /home/stooj/.config/user-dirs.locale
    - source: {{ files_switch(['user-dirs.locale.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-xdg-config-file-xdg-user-dirs-locale-conf-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - context:
        xdg: {{ stooj_dotfiles_workstation_common.xdg | json }}
