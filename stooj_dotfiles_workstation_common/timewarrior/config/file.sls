# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set repo = stooj_dotfiles_workstation_common.timewarrior.get('repo', None) %}
{%- set target = stooj_dotfiles_workstation_common.timewarrior.get('target', None) %}

stooj-dotfiles-workstation-common-timewarrior-config-repo-env-file-managed:
  file.managed:
    - name: /home/stooj/.config/zsh/env_includes/timewarrior.zsh
    - source: {{ files_switch(['timewarrior.zsh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-timewarrior-config-repo-env-file-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        timewarrior: {{ stooj_dotfiles_workstation_common.timewarrior | yaml }}
        target: {{ target }}

stooj-dotfiles-workstation-common-timewarrior-config-repo-timewarrior-cloned:
  git.latest:
    - name: {{ repo }}
    - target: {{ target }}
    - user: stooj
    {%- if repo != 'https://github.com/saltstack-formulas/template-formula.git' %}
    {# This is to allow tests to timewarrior without access to a real ssh key #}
    - identity:
      - salt://ssh/files/deploy_key
    {%- endif %}
