# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set repo = stooj_dotfiles_workstation_common.timewarrior.get('repo', None) %}
{%- set target = stooj_dotfiles_workstation_common.timewarrior.get('target', None) %}

stooj-dotfiles-workstation-common-timewarrior-config-repo-env-file-absent:
  file.absent:
    - name: /home/stooj/.config/zsh/env_includes/timewarrior.zsh

stooj-dotfiles-workstation-common-timewarrior-config-repo-timewarrior-absent:
  file.absent:
    - name: {{ repo }}

