# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-autostart-config-file-autostart-dir-managed:
  file.directory:
    - name: /home/stooj/.config/autostart
    - user: stooj
    - group: stooj
    - mode: 700
    - makedirs: True

stooj-dotfiles-workstation-common-autostart-config-file-autostart-gnome-keyring-ssh-managed:
  file.managed:
    - name: /home/stooj/.config/autostart/gnome-keyring-ssh.desktop
    - source: {{ files_switch(['gnome-keyring-ssh.desktop.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-autostart-config-file-autostart-gnome-keyring-ssh-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        autostart: {{ stooj_dotfiles_workstation_common.autostart | json }}
    - require:
      - stooj-dotfiles-workstation-common-autostart-config-file-autostart-dir-managed
