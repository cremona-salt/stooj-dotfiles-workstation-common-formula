# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-autostart-config-file-autostart-gnome-keyring-ssh-absent:
  file.absent:
    - name: /home/stooj/.config/autostart/gnome-keyring-ssh.desktop

stooj-dotfiles-workstation-common-autostart-config-file-autostart-dir-absent:
  file.absent:
    - name: /home/stooj/.config/autostart
