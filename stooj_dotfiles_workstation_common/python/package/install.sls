# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}

{# The salt installer creates /usr/bin/chardetect as part of the installation.#
 # This prevents python-chardet from being installed by pacman.               #
 # Deleting the file before attempting the installation should hopefully      #
 # allow us to install python-chardet                                         #}

stooj-dotfiles-workstation-common-python-package-install-remove-chardet:
  file.absent:
    - name: /usr/bin/chardetect
    - onlyif:
      - result=$(pacman -Qo /usr/bin/chardetect 2>&1); [ "$result" == "error: No package owns /usr/bin/chardetect" ]

stooj-dotfiles-workstation-common-python-package-install-install-chardet:
  pkg.installed:
    - name: python-chardet
    - onchanges:
      - stooj-dotfiles-workstation-common-python-package-install-remove-chardet

stooj-dotfiles-workstation-common-python-package-install-pkg-installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles_workstation_common.python.pkgs | yaml }}
    - require:
      - stooj-dotfiles-workstation-common-python-package-install-remove-chardet
