# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-python-config-file-venvwrapper-config-managed:
  file.managed:
    - name: /home/stooj/.config/zsh/rc_includes/virtualenv.zsh
    - source: {{ files_switch(['virtualenv.zsh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-python-config-file-venvwrapper-config-managed',
			      use_subpath=True
			     )
	      }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        python: {{ stooj_dotfiles_workstation_common.python | json }}
