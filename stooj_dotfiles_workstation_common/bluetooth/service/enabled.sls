# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}

stooj-dotfiles-workstation-common-bluetooth-service-enabled-service-running:
  service.running:
    - name: {{ stooj_dotfiles_workstation_common.bluetooth.service.name }}
    - enable: True
