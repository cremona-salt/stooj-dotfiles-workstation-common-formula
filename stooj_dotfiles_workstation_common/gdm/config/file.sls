# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set tplsubname = tpldir.split('/')[1] %}
{%- set tplsubroot = tplroot ~ '.' ~ tplsubname %}
{%- set sls_package_install = tplsubroot ~ '.package.install' %}

include:
  - {{ sls_package_install }}

stooj-dotfiles-workstation-common-gdm-config-file-accountsservice-dir-managed:
  file.directory:
    - name: /var/lib/AccountsService
    - user: root
    - group: root
    - mode: 755
    - makedirs: True

stooj-dotfiles-workstation-common-gdm-config-file-accountsservice-users-dir-managed:
  file.directory:
    - name: /var/lib/AccountsService/users
    - user: root
    - group: root
    - mode: 700
    - require:
      - stooj-dotfiles-workstation-common-gdm-config-file-accountsservice-dir-managed

stooj-dotfiles-workstation-common-gdm-config-file-accountsservice-icons-dir-managed:
  file.directory:
    - name: /var/lib/AccountsService/icons
    - user: root
    - group: root
    - mode: 700
    - require:
      - stooj-dotfiles-workstation-common-gdm-config-file-accountsservice-dir-managed

stooj-dotfiles-workstation-common-gdm-config-file-user-config-managed:
  file.managed:
    - name: /var/lib/AccountsService/users/stooj
    - source: {{ files_switch(['gdm-user.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-gdm-config-file-user-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: root
    - group: root
    - template: jinja
    - context:
        gdm: {{ stooj_dotfiles_workstation_common.gdm | json }}
    - require:
      - stooj-dotfiles-workstation-common-gdm-config-file-accountsservice-users-dir-managed

{#- https://wiki.archlinux.org/index.php/Bluetooth_headset#Gnome_with_GDM #}
stooj-dotfiles-workstation-common-gdm-config-file-pulse-dir-managed:
  file.directory:
    - name: /var/lib/gdm/.config/pulse/
    - user: gdm
    - group: gdm
    - mode: 700
    - makedirs: True

stooj-dotfiles-workstation-common-gdm-config-file-pulse-file-managed:
  file.managed:
    - name: /var/lib/gdm/.config/pulse/client.conf
    - source: {{ files_switch(['pulse-client.conf.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-gdm-config-file-pulse-file-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: root
    - group: root
    - template: jinja
    - context:
        gdm: {{ stooj_dotfiles_workstation_common.gdm | json }}
    - require:
      - stooj-dotfiles-workstation-common-gdm-config-file-pulse-dir-managed

stooj-dotfiles-workstation-common-gdm-config-file-systemd-dir-managed:
  file.directory:
    - name: /var/lib/gdm/.config/systemd/user
    - user: gdm
    - group: gdm
    - mode: 700
    - makedirs: True

stooj-dotfiles-workstation-common-gdm-config-file-systemd-pulse-socket-disabled:
  file.symlink:
    - name: /var/lib/gdm/.config/systemd/user/pulseaudio.socket
    - target: /dev/null
    - user: gdm
    - group: gdm
    - require:
      - stooj-dotfiles-workstation-common-gdm-config-file-systemd-dir-managed
