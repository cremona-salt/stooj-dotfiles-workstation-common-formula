# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}

stooj-dotfiles-workstation-common-gdm-service-enabled-service-enabled:
  service.enabled:
    - name: {{ stooj_dotfiles_workstation_common.gdm.service.name }}
