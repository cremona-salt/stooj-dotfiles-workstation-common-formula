# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-pacman-config-file-config-managed:
  file.managed:
    - name: /etc/pacman.conf
    - source: {{ files_switch(['pacman.conf.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-pacman-config-file-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: root
    - group: root
    - template: jinja
    - context:
        pacman: {{ stooj_dotfiles_workstation_common.pacman | json }}

stooj-dotfiles-workstation-common-pacman-config-file-pacman-updated:
  cmd.run:
    - name: pacman --sync --refresh
    - onchanges:
      - stooj-dotfiles-workstation-common-pacman-config-file-config-managed
