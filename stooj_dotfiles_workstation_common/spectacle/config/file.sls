# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-spectacle-config-file-spectacle-conf-managed:
  file.managed:
    - name: /home/stooj/.config/spectaclerc
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - replace: False

stooj-dotfiles-workstation-common-spectacle-config-file-spectacle-conf-content-managed:
  ini.options_present:
    - name: /home/stooj/.config/spectaclerc
    - sections:
        Save:
          saveFilenameFormat: '%Y-%M-%D-%H%M%S_%T_%d'
    - require:
      - stooj-dotfiles-workstation-common-spectacle-config-file-spectacle-conf-managed
