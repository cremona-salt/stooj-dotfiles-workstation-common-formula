# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}

stooj-dotfiles-workstation-common-youtube-dl-package-clean-package-absent:
  pkg.removed:
    - pkgs: {{ stooj_dotfiles_workstation_common.youtube_dl.pkgs | yaml }}
