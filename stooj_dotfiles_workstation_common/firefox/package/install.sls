# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-firefox-package-install-pkg-installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles_workstation_common.firefox.pkgs | yaml }}

{%- for profile in stooj_dotfiles_workstation_common.firefox.profiles %}
stooj-dotfiles-workstation-common-firefox-config-file-firefox-profile-{{ profile }}-launcher-managed:
  file.managed:
    - name: /home/stooj/bin/{{ profile }}-firefox
    - source: {{ files_switch(['launch-firefox-profile.sh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-firefox-config-file-firefox-profile-launcher-managed',
                              use_subpath=True
                             )
              }}
    - mode: 700
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        firefox: {{ stooj_dotfiles_workstation_common.firefox | json }}
        profile: {{ profile }}
    - makedirs: True
{%- endfor %}
