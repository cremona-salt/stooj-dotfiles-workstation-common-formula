# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-xorg-config-file-xinit-config-managed:
  file.managed:
    - name: /home/stooj/.xinitrc
    - source: {{ files_switch(['xinitrc.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-xorg-config-file-xinit-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        xorg: {{ stooj_dotfiles_workstation_common.xorg | json }}

stooj-dotfiles-workstation-common-xorg-config-file-xresources-config-managed:
  file.managed:
    - name: /home/stooj/.config/Xresources
    - mode: 600
    - user: stooj
    - group: stooj
    - replace: False

stooj-dotfiles-workstation-common-xorg-config-file-xresources-dir-managed:
  file.directory:
    - name: /home/stooj/.config/Xresources.d
    - user: stooj
    - group: stooj
    - mode: 700
    - makedirs: True

stooj-dotfiles-workstation-common-xorg-config-file-xresources-scaling-enabled:
  file.append:
    - name: /home/stooj/.config/Xresources
    - text: '#include "/home/stooj/.config/Xresources.d/scaling"'
    - require:
      - stooj-dotfiles-workstation-common-xorg-config-file-xresources-config-managed

stooj-dotfiles-workstation-common-xorg-config-file-xresources-scaling-managed:
  file.managed:
    - name: /home/stooj/.config/Xresources.d/scaling
    - source: {{ files_switch(['scaling.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-xorg-config-file-xresources-scaling-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        xorg: {{ stooj_dotfiles_workstation_common.xorg | json }}
        scaling_factor: {{ salt.grains.get('scaling_factor', 1 ) }}
