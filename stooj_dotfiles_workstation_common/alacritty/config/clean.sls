# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-alacritty-config-file-env-includes-config-absent:
  file.absent:
    - name: /home/stooj/.config/zsh/env_includes/alacritty.zsh

stooj-dotfiles-workstation-common-alacritty-config-file-gruvbox-dark-theme-absent:
  file.absent:
    - name: /home/stooj/.local/share/alacritty/gruvbox-dark.yml

stooj-dotfiles-workstation-common-alacritty-config-file-alacritty-absent:
  file.absent:
    - name: /home/stooj/.config/alacritty/alacritty.yml

stooj-dotfiles-workstation-common-alacritty-config-file-dir-absent:
  file.absent:
    - name: /home/stooj/.config/alacritty
