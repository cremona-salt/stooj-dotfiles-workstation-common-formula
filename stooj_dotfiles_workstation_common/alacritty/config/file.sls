# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}


stooj-dotfiles-workstation-common-alacritty-config-file-dir-managed:
  file.directory:
    - name: /home/stooj/.config/alacritty
    - mode: 700
    - user: stooj
    - group: stooj

stooj-dotfiles-workstation-common-alacritty-config-file-alacritty-managed:
  file.managed:
    - name: /home/stooj/.config/alacritty/alacritty.yml
    - source: {{ files_switch(['alacritty.yml.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-alacritty-config-file-alacritty-conf-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - context:
        alacritty: {{ stooj_dotfiles_workstation_common.alacritty | json }}
    - require:
      - stooj-dotfiles-workstation-common-alacritty-config-file-alacritty-dir-managed

stooj-dotfiles-workstation-common-alacritty-config-file-gruvbox-dark-theme-managed:
  file.managed:
    - name: /home/stooj/.local/share/alacritty/gruvbox-dark.yml
    - source: {{ files_switch(['gruvbox-dark.yml.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-alacritty-config-file-gruvbox-dark-theme-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        alacritty: {{ stooj_dotfiles_workstation_common.alacritty | json }}

stooj-dotfiles-workstation-common-alacritty-config-file-env-includes-config-managed:
  file.managed:
    - name: /home/stooj/.config/zsh/env_includes/alacritty.zsh
    - source: {{ files_switch(['alacritty.zsh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-alacritty-config-file-env-includes-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        alacritty: {{ stooj_dotfiles_workstation_common.alacritty | json }}
