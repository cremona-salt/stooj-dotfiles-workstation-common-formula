# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-face-config-file-face-absent:
  file.absent:
    - name: /home/stooj/.face

stooj-dotfiles-workstation-common-face-config-file-face-symlink-absent:
  file.absent:
    - name: /home/stooj/.face.icon
