# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-tmate-config-file-tmate-dir-managed:
  file.directory:
    - name: /home/stooj/.config/tmate
    - mode: 700
    - user: stooj
    - group: stooj

stooj-dotfiles-workstation-common-tmate-config-file-tmate-conf-managed:
  file.managed:
    - name: /home/stooj/.config/tmate/tmate.conf
    - source: {{ files_switch(['tmate.conf.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-tmate-config-file-tmate-conf-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - context:
        tmate: {{ stooj_dotfiles_workstation_common.tmate | json }}
    - require:
      - stooj-dotfiles-workstation-common-tmate-config-file-tmate-dir-managed
