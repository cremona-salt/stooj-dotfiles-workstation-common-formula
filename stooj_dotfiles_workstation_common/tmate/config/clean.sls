# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-tmate-config-file-tmate-conf-absent:
  file.absent:
    - name: /home/stooj/.config/tmate/tmate.conf

stooj-dotfiles-workstation-common-tmate-config-file-tmate-dir-absent:
  file.absent:
    - name: /home/stooj/.config/tmate
