# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set tplsubname = tpldir.split('/')[1] %}
{%- set tplsubroot = tplroot ~ '.' ~ tplsubname %}
{%- set sls_package_install = tplsubroot ~ '.package.install' %}

include:
  - {{ sls_package_install }}

{%- set ssid = stooj_dotfiles_workstation_common.wireless.ssid %}
stooj-dotfiles-workstation-common-wireless-config-file-network-added:
  file.managed:
    - name: /etc/NetworkManager/system-connections/{{ ssid }}.nmconnection
    - source: {{ files_switch(['wireless.nmconnection.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-wireless-config-file-network-added',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: root
    - group: root
    - template: jinja
    - context:
        wireless: {{ stooj_dotfiles_workstation_common.wireless | json }}
    - require:
      - sls: {{ sls_package_install }}
