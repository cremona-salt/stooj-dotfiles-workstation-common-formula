# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-tridactyl-config-file-gruvbox-dark-theme-absent:
  file.absent:
    - name: /home/stooj/.config/tridactyl/themes/base16-gruvbox-dark-soft.css

stooj-dotfiles-workstation-common-tridactyl-config-file-tridactyl-themes-dir-absent:
  file.absent:
    - name: /home/stooj/.config/tridactyl/themes

stooj-dotfiles-workstation-common-tridactyl-config-file-tridactyl-file-absent:
  file.absent:
    - name: /home/stooj/.config/tridactyl/tridactylrc

stooj-dotfiles-workstation-common-tridactyl-config-file-tridactyl-dir-absent:
  file.absent:
    - name: /home/stooj/.config/tridactyl
