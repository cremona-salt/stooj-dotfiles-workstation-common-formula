# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}


stooj-dotfiles-workstation-common-tridactyl-config-file-tridactyl-dir-managed:
  file.directory:
    - name: /home/stooj/.config/tridactyl
    - mode: 700
    - user: stooj
    - group: stooj

stooj-dotfiles-workstation-common-tridactyl-config-file-tridactyl-file-managed:
  file.managed:
    - name: /home/stooj/.config/tridactyl/tridactylrc
    - source: {{ files_switch(['tridactylrc.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-tridactyl-config-file-tridactyl-file-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        tridactyl: {{ stooj_dotfiles_workstation_common.tridactyl | json }}
        favourites: {{ stooj_dotfiles_workstation_common.favourites | json }}
    - require:
      - stooj-dotfiles-workstation-common-tridactyl-config-file-tridactyl-dir-managed

stooj-dotfiles-workstation-common-tridactyl-config-file-tridactyl-themes-dir-managed:
  file.directory:
    - name: /home/stooj/.config/tridactyl/themes
    - mode: 700
    - user: stooj
    - group: stooj

stooj-dotfiles-workstation-common-tridactyl-config-file-gruvbox-dark-theme-managed:
  file.managed:
    - name: /home/stooj/.config/tridactyl/themes/base16-gruvbox-dark-soft.css
    - source: {{ files_switch(['base16-gruvbox-dark-soft.css.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-tridactyl-config-file-tridactyl-file-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        tridactyl: {{ stooj_dotfiles_workstation_common.tridactyl | json }}
    - require:
      - stooj-dotfiles-workstation-common-tridactyl-config-file-tridactyl-themes-dir-managed
