# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - stooj_dotfiles_common
  - .about
  - .acpi
  - .acpilight
  - .alacritty
  - .asdf_vm
  - .autostart
  - .cheese
  - .chromium
  - .code
  - .cron
  - .ddrescue
  - .directories
  - .doctl
  - .dvd_playback
  - .element_desktop
  - .face
  - .firefox
  - .fonts
  - .gdm
  - .gimp
  - .golang
  - .gpg
  - .inetutils
  - .inkscape
  - .i3
  - .java
  - .kdeconnect
  - .konsole
  - .librecad
  - .libvirt_extras
  - .man_db
  - .mlocate
  - .mpv
  - .neovim_qt
  - .nmap
  - .ntfs_3g
  - .pacman
  - .pam_environment
  - .pandoc
  - .pass
  - .powerlevel10k
  - .pre_commit
  - .profile
  - .proselint
  - .python
  - .qcad
  - .qutebrowser
  - .rstcheck
  - .scrot
  - .shellcheck
  - .signal
  - .spectacle
  - .sphinx
  - .sound
  - .sshfs
  - .taskwarrior
  - .timewarrior
  - .terraform
  - .texlive
  - .tmate
  - .tridactyl
  - .vagrant
  - .virtualbox
  - .xdg
  - .xorg
  - .xrandr
  - .youtube_dl
  - .yubikey
  - .zathura
  {%- if salt.grains.get('bluetooth', False) %}
  - .bluetooth
  {%- endif %}
  {%- if salt.grains.get('battery', False) %}
  - .check_battery
  {%- endif %}
  {%- if salt.grains.get('form_factor', None) == 'laptop' %}
  - .tlp
  {%- endif %}
  {%- if salt.grains.get('wireless', False) %}
  - .wireless
  {%- endif %}
