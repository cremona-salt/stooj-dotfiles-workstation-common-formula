# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-mpv-config-file-mpv-dir-managed:
  file.directory:
    - name: /home/stooj/.config/mpv
    - mode: 700
    - user: stooj
    - group: stooj

stooj-dotfiles-workstation-common-mpv-config-file-mpv-conf-managed:
  file.managed:
    - name: /home/stooj/.config/mpv/mpv.conf
    - source: {{ files_switch(['mpv.conf.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-mpv-config-file-mpv-conf-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - context:
        mpv: {{ stooj_dotfiles_workstation_common.mpv | json }}
    - require:
      - stooj-dotfiles-workstation-common-mpv-config-file-mpv-dir-managed
