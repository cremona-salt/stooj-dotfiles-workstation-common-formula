# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-mpv-config-file-mpv-conf-absent:
  file.absent:
    - name: /home/stooj/.config/mpv/mpv.conf

stooj-dotfiles-workstation-common-mpv-config-file-mpv-dir-absent:
  file.absent:
    - name: /home/stooj/.config/mpv
