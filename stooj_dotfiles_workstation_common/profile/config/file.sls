# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-profile-config-file-env-config-managed:
  file.managed:
    - name: /home/stooj/.profile
    - source: {{ files_switch(['profile.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-profile-config-file-env-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        profile: {{ stooj_dotfiles_workstation_common.profile | json }}
