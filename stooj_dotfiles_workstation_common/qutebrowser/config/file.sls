# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set sls_python_present = tplroot ~ '.python.package.install' %}

include:
  - {{ sls_python_present }}

stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-dir-managed:
  file.directory:
    - name: /home/stooj/.config/qutebrowser
    - mode: 700
    - user: stooj
    - group: stooj

stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-bookmarks-dir-managed:
  file.directory:
    - name: /home/stooj/.config/qutebrowser/bookmarks
    - mode: 700
    - user: stooj
    - group: stooj
    - require:
      - stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-dir-managed

stooj-dotfiles-workstation-common-qutebrowser-config-file-config-config-managed:
  file.managed:
    - name: /home/stooj/.config/qutebrowser/config.py
    - source: {{ files_switch(['config.py.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-qutebrowser-config-file-config-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        qutebrowser: {{ stooj_dotfiles_workstation_common.qutebrowser | json }}
        favourites: {{ stooj_dotfiles_workstation_common.favourites | json }}
    - require:
      - stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-dir-managed

stooj-dotfiles-workstation-common-qutebrowser-config-file-keys-config-managed:
  file.managed:
    - name: /home/stooj/.config/qutebrowser/keys.conf
    - source: {{ files_switch(['keys.conf.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-qutebrowser-config-file-keys-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        qutebrowser: {{ stooj_dotfiles_workstation_common.qutebrowser | json }}
    - require:
      - stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-dir-managed

stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-config-managed:
  file.managed:
    - name: /home/stooj/.config/qutebrowser/qutebrowser.conf
    - source: {{ files_switch(['qutebrowser.conf.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        qutebrowser: {{ stooj_dotfiles_workstation_common.qutebrowser | json }}
    - require:
      - stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-dir-managed

stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-scripts-repo-managed:
  git.latest:
    - name: {{ stooj_dotfiles_workstation_common.qutebrowser.python_script_repo }}
    - target: /home/stooj/code/qutebrowser-scripts
    - user: stooj
    {%- if stooj_dotfiles_workstation_common.qutebrowser.python_script_repo.startswith('git') %}
    - identity:
      - salt://ssh/files/deploy_key
    {%- endif %}

stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-scripts-repo-group-ownership-managed:
  file.directory:
    - name: /home/stooj/code/qutebrowser-scripts
    - group: stooj
    - recurse:
      - group
    - require:
      - stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-scripts-repo-managed

stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-script-dir-managed:
  file.directory:
    - name: /home/stooj/.local/share/qutebrowser
    - mode: 700
    - user: stooj
    - group: stooj

stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-userscripts-dir-managed:
  file.directory:
    - name: /home/stooj/.local/share/qutebrowser/userscripts
    - mode: 700
    - user: stooj
    - group: stooj
    - require:
      - stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-script-dir-managed

{%- for script in stooj_dotfiles_workstation_common.qutebrowser.python_scripts %}
stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-script-{{ script }}-managed:
  file.managed:
    - name: /home/stooj/.local/share/qutebrowser/userscripts/{{ script }}
    - source: {{ files_switch(['python_scripts/' ~ script ~ '.sh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-script-' ~ script ~ '-managed',
                              use_subpath=True
                             )
              }}
    - mode: 700
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        qutebrowser: {{ stooj_dotfiles_workstation_common.qutebrowser | json }}
    - require:
      - stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-script-dir-managed
{%- endfor %}

stooj-dotfiles-workstation-common-qutebrowser-config-file-qutescript-repo-cloned:
  git.latest:
    - name: https://github.com/hiway/python-qutescript.git
    - target: /tmp/python-qutescript
    - user: stooj
    - unless: ls /home/stooj/.local/lib/python3.8/site-packages/qutescript

stooj-dotfiles-workstation-common-qutebrowser-config-file-qutescript-repo-installed:
  cmd.run:
    - name: pip install --user /tmp/python-qutescript
    - cwd: /tmp/python-qutescript
    - runas: stooj
    - group: stooj
    - require:
      - sls: {{ sls_python_present }}
    - creates:
      - /home/stooj/.local/lib/python3.8/site-packages/qutescript

stooj-dotfiles-workstation-common-qutebrowser-config-file-quickmarks-config-managed:
  file.managed:
    - name: /home/stooj/.config/qutebrowser/quickmarks
    - source: {{ files_switch(['quickmarks.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-qutebrowser-config-file-quickmarks-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        qutebrowser: {{ stooj_dotfiles_workstation_common.qutebrowser | json }}

stooj-dotfiles-workstation-common-qutebrowser-config-file-bookmarks-urls-managed:
  file.managed:
    - name: /home/stooj/.config/qutebrowser/bookmarks/urls
    - source: {{ files_switch(['bookmarks.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-qutebrowser-config-file-bookmarks-urls-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        qutebrowser: {{ stooj_dotfiles_workstation_common.qutebrowser | json }}

{%- for profile in stooj_dotfiles_workstation_common.qutebrowser.profiles %}
stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-profile-{{ profile }}-launcher-managed:
  file.managed:
    - name: /home/stooj/bin/{{ profile }}-qutebrowser
    - source: {{ files_switch(['launch-qutebrowser-profile.sh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-profile-launcher-managed',
                              use_subpath=True
                             )
              }}
    - mode: 700
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        qutebrowser: {{ stooj_dotfiles_workstation_common.qutebrowser | json }}
        profile: {{ profile }}
    - makedirs: True
{%- endfor %}
