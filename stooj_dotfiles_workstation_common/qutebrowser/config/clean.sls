# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- for profile in stooj_dotfiles_workstation_common.qutebrowser.profiles %}
stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-profile-{{ profile }}-launcher-absent:
  file.absent:
    - name: /home/stooj/bin/{{ profile }}-qutebrowser
{%- endfor %}

stooj-dotfiles-workstation-common-qutebrowser-config-file-bookmarks-urls-absent:
  file.absent:
    - name: /home/stooj/.config/qutebrowser/bookmarks/urls

stooj-dotfiles-workstation-common-qutebrowser-config-file-quickmarks-config-absent:
  file.absent:
    - name: /home/stooj/.config/qutebrowser/quickmarks

stooj-dotfiles-workstation-common-qutebrowser-config-file-qutescript-repo-absent:
  file.absent:
    - name: /home/stooj/.local/lib/python3.8/site-packages/qutescript

stooj-dotfiles-workstation-common-qutebrowser-config-file-qutescript-repo-cloned-absent:
  file.absent:
    - name: /tmp/python-qutescript

{%- for script in stooj_dotfiles_workstation_common.qutebrowser.python_scripts %}
stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-script-{{ script }}-absent:
  file.absent:
    - name: /home/stooj/.local/share/qutebrowser/userscripts/{{ script }}
{%- endfor %}

stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-script-dir-absent:
  file.absent:
    - name: /home/stooj/.local/share/qutebrowser

stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-scripts-repo-absent:
  file.absent:
    - name: /home/stooj/code/qutebrowser-scripts

stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-config-absent:
  file.absent:
    - name: /home/stooj/.config/qutebrowser/qutebrowser.conf

stooj-dotfiles-workstation-common-qutebrowser-config-file-keys-config-absent:
  file.absent:
    - name: /home/stooj/.config/qutebrowser/keys.conf

stooj-dotfiles-workstation-common-qutebrowser-config-file-config-config-absent:
  file.absent:
    - name: /home/stooj/.config/qutebrowser/config.py

stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-bookmarks-dir-absent:
  file.absent:
    - name: /home/stooj/.config/qutebrowser/bookmarks

stooj-dotfiles-workstation-common-qutebrowser-config-file-qutebrowser-dir-absent:
  file.absent:
    - name: /home/stooj/.config/qutebrowser
