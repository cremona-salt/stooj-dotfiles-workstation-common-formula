# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}

stooj-dotfiles-workstation-common-qutebrowser-package-clean-package-absent:
  pkg.removed:
    - pkgs: {{ stooj_dotfiles_workstation_common.qutebrowser.pkgs | yaml }}

stooj-dotfiles-workstation-common-qutebrowser-package-install-wrapper-script-absent:
  file.absent:
    - name: /home/stooj/bin/qutebrowser
