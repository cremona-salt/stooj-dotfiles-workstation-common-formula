# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-qutebrowser-package-install-pkg-installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles_workstation_common.qutebrowser.pkgs | yaml }}

stooj-dotfiles-workstation-common-qutebrowser-package-install-wrapper-script-installed:
  file.managed:
    - name: /home/stooj/bin/qutebrowser
    - source: {{ files_switch(['qutebrowser-wrapper-script.sh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-qutebrowser-package-install-wrapper-script-installed',
                              use_subpath=True
                             )
              }}
    - mode: 700
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        qutebrowser: {{ stooj_dotfiles_workstation_common.qutebrowser | json }}
