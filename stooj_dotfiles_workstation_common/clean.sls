# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  {%- if salt.grains.get('wireless', False) %}
  - .wireless.clean
  {%- endif %}
  {%- if salt.grains.get('form_factor', None) == 'laptop' %}
  - .tlp.clean
  {%- endif %}
  {%- if salt.grains.get('battery', False) %}
  - .check-battery.clean
  {%- endif %}
  {%- if salt.grains.get('bluetooth', False) %}
  - .bluetooth.clean
  {%- endif %}
  - .zathura.clean
  - .yubikey.clean
  - .youtube_dl.clean
  - .xrandr.clean
  - .xorg.clean
  - .xdg.clean
  - .virtualbox.clean
  - .vagrant.clean
  - .tridactyl.clean
  - .tmate.clean
  - .texlive.clean
  - .terraform.clean
  - .timewarrior.clean
  - .taskwarrior.clean
  - .sshfs.clean
  - .sphinx.clean
  - .spectacle.clean
  - .sound.clean
  - .signal.clean
  - .shellcheck.clean
  - .scrot.clean
  - .rstcheck.clean
  - .qutebrowser.clean
  - .qcad.clean
  - .python.clean
  - .proselint.clean
  - .profile.clean
  - .pre_commit.clean
  - .powerlevel10k.clean
  - .pass.clean
  - .pandoc.clean
  - .pam_environment.clean
  - .ntfs_3g.clean
  - .nmap.clean
  - .neovim_qt.clean
  - .mpv.clean
  - .mlocate.clean
  - .man_db.clean
  - .libvirt_extras.clean
  - .librecad.clean
  - .konsole.clean
  - .kdeconnect.clean
  - .java.clean
  - .inkscape.clean
  - .inetutils.clean
  - .i3.clean
  - .gpg.clean
  - .golang.clean
  - .gimp.clean
  - .gdm.clean
  - .fonts.clean
  - .firefox.clean
  - .face.clean
  - .element_desktop.clean
  - .dvd_playback.clean
  - .doctl.clean
  - .directories.clean
  - .ddrescue.clean
  - .cron.clean
  - .code.clean
  - .chromium.clean
  - .cheese.clean
  - .autostart.clean
  - .asdf_vm.clean
  - .alacritty.clean
  - .acpilight.clean
  - .acpi.clean
  - .about.clean
  - stooj_dotfiles_common.clean
