# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}

stooj-dotfiles-workstation-common-i3-i3wm-config-file-script-exit-absent:
  file.absent:
    - name: /home/stooj/.config/i3/scripts/exit_menu

stooj-dotfiles-workstation-common-i3-i3wm-config-file-script-dir-absent:
  file.absent:
    - name: /home/stooj/.config/i3/scripts

stooj-dotfiles-workstation-common-i3-i3wm-config-clean-config-absent:
  file.absent:
    - name: /home/stooj/.config/i3/config

stooj-dotfiles-workstation-common-i3-i3wm-config-clean-dir-absent:
  file.absent:
    - name: /home/stooj/.config/i3

stooj-dotfiles-workstation-common-i3-i3wm-config-file-autostart-dir-absent:
  file.absent:
    - name: /home/stooj/.config/i3/autostart.d

stooj-dotfiles-workstation-common-i3-i3wm-config-file-autostart-script-absent:
  file.absent:
    - name: /home/stooj/.config/i3/autostart
