# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set tplsubname = tpldir.split('/')[1] %}
{%- set tplsubsubname = tpldir.split('/')[2] %}
{%- set tplsubsubroot = tplroot ~ '.' ~ tplsubname ~ '.' ~ tplsubsubname %}
{%- set sls_package_install = tplsubsubroot ~ '.package.install' %}

include:
  - {{ sls_package_install }}

stooj-dotfiles-workstation-common-i3-i3wm-config-file-dir-managed:
  file.directory:
    - name: /home/stooj/.config/i3
    - user: stooj
    - group: stooj
    - mode: 755
    - makedirs: True

stooj-dotfiles-workstation-common-i3-i3wm-config-file-autostart-dir-managed:
  file.directory:
    - name: /home/stooj/.config/i3/autostart.d
    - user: stooj
    - group: stooj
    - mode: 700
    - makedirs: True
    - require:
      - stooj-dotfiles-workstation-common-i3-i3wm-config-file-dir-managed

stooj-dotfiles-workstation-common-i3-i3wm-config-file-config-managed:
  file.managed:
    - name: /home/stooj/.config/i3/config
    - source: {{ files_switch(['i3wm/i3config.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-i3-i3wm-config-file-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        i3wm: {{ stooj_dotfiles_workstation_common.i3.i3wm | json }}
    - require:
      - stooj-dotfiles-workstation-common-i3-i3wm-config-file-dir-managed

stooj-dotfiles-workstation-common-i3-i3wm-config-file-autostart-script-managed:
  file.managed:
    - name: /home/stooj/.config/i3/autostart
    - source: {{ files_switch(['i3wm/autostart.sh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-i3-i3wm-config-file-autostart-script-managed',
                              use_subpath=True
                             )
              }}
    - mode: 700
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        i3wm: {{ stooj_dotfiles_workstation_common.i3.i3wm | json }}
    - require:
      - stooj-dotfiles-workstation-common-i3-i3wm-config-file-dir-managed

stooj-dotfiles-workstation-common-i3-i3wm-config-file-script-dir-managed:
  file.directory:
    - name: /home/stooj/.config/i3/scripts
    - user: stooj
    - group: stooj
    - mode: 700
    - makedirs: True
    - require:
      - stooj-dotfiles-workstation-common-i3-i3wm-config-file-dir-managed

stooj-dotfiles-workstation-common-i3-i3wm-config-file-script-exit-managed:
  file.managed:
    - name: /home/stooj/.config/i3/scripts/exit_menu
    - source: {{ files_switch(['i3wm/scripts/exit_menu.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-i3-i3wm-config-file-script-exit-managed',
                              use_subpath=True
                             )
              }}
    - mode: 700
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        i3wm: {{ stooj_dotfiles_workstation_common.i3.i3wm | json }}
    - require:
      - stooj-dotfiles-workstation-common-i3-i3wm-config-file-script-dir-managed

stooj-dotfiles-workstation-common-i3-i3wm-config-file-toggle-keyboard-script-managed:
  file.managed:
    - name: /home/stooj/bin/toggle-keyboard
    - source: {{ files_switch(['i3wm/toggle-keyboard.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-i3-i3wm-config-file-toggle-keyboard-script-managed',
                              use_subpath=True
                             )
              }}
    - mode: 700
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        i3wm: {{ stooj_dotfiles_workstation_common.i3.i3wm | json }}
