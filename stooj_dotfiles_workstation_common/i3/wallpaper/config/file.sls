# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-i3-wallpaper-config-file-service-dir-managed:
  file.directory:
    - name: /home/stooj/.config/systemd/user
    - user: stooj
    - group: stooj
    - mode: 755
    - makedirs: True

stooj-dotfiles-workstation-common-i3-wallpaper-config-file-service-unit-file-managed:
  file.managed:
    - name: /home/stooj/.config/systemd/user/change_i3_background.service
    - source: {{ files_switch(['wallpaper/change_i3_background.service.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-i3-wallpaper-config-file-service-unitfile-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        wallpaper: {{ stooj_dotfiles_workstation_common.i3.wallpaper | json }}
    - require:
      - stooj-dotfiles-workstation-common-i3-wallpaper-config-file-service-dir-managed

stooj-dotfiles-workstation-common-i3-wallpaper-config-file-service-unit-timer-managed:
  file.managed:
    - name: /home/stooj/.config/systemd/user/change_i3_background.timer
    - source: {{ files_switch(['wallpaper/change_i3_background.timer.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-i3-wallpaper-config-file-service-unit-timer-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        wallpaper: {{ stooj_dotfiles_workstation_common.i3.wallpaper | json }}
    - require:
      - stooj-dotfiles-workstation-common-i3-wallpaper-config-file-service-dir-managed

stooj-dotfiles-workstation-common-i3-wallpaper-config-file-bin-script-managed:
  file.managed:
    - name: /home/stooj/bin/change_wallpaper
    - source: {{ files_switch(['wallpaper/change_wallpaper.py.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-i3-wallpaper-config-file-bin-script-managed',
                              use_subpath=True
                             )
              }}
    - mode: 700
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        wallpaper: {{ stooj_dotfiles_workstation_common.i3.wallpaper | json }}

stooj-dotfiles-workstation-common-i3-wallpaper-config-file-symlink-managed:
  file.symlink:
    - name: /home/stooj/.config/desktop-backgrounds
    - target: {{ stooj_dotfiles_workstation_common.i3.wallpaper.location }}
    - user: stooj
    - group: stooj
    - mode: 755
