# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}

stooj-dotfiles-workstation-common-i3-wallpaper-config-file-service-unit-file-absent:
  file.absent:
    - name: /home/stooj/.config/systemd/user/change_i3_background.service

stooj-dotfiles-workstation-common-i3-wallpaper-config-file-service-unit-timer-absent:
  file.absent:
    - name: /home/stooj/.config/systemd/user/change_i3_background.timer

stooj-dotfiles-workstation-common-i3-wallpaper-config-file-bin-script-absent:
  file.absent:
    - name: /home/stooj/bin/change_wallpaper

stooj-dotfiles-workstation-common-i3-wallpaper-config-file-symlink-absent:
  file.absent:
    - name: /home/stooj/.config/desktop-backgrounds
