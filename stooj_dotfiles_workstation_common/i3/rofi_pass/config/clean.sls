# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}

stooj-dotfiles-workstation-common-i3-rofi-pass-config-clean-config-absent:
  file.absent:
    - name: /home/stooj/.config/rofi-pass/config

stooj-dotfiles-workstation-common-i3-rofi-pass-config-clean-dir-absent:
  file.absent:
    - name: /home/stooj/.config/rofi-pass
