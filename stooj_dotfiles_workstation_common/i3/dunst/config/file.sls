# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set tplsubname = tpldir.split('/')[1] %}
{%- set tplsubsubname = tpldir.split('/')[2] %}
{%- set tplsubsubroot = tplroot ~ '.' ~ tplsubname ~ '.' ~ tplsubsubname %}
{%- set sls_package_install = tplsubsubroot ~ '.package.install' %}

include:
  - {{ sls_package_install }}

stooj-dotfiles-workstation-common-i3-dunst-config-file-dir-managed:
  file.directory:
    - name: /home/stooj/.config/dunst
    - user: stooj
    - group: stooj
    - mode: 755
    - makedirs: True

stooj-dotfiles-workstation-common-i3-dunst-config-file-config-managed:
  file.managed:
    - name: /home/stooj/.config/dunst/dunstrc
    - source: {{ files_switch(['dunst/dunstrc.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-i3-dunst-config-file-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        dunst: {{ stooj_dotfiles_workstation_common.i3.dunst | json }}
    - require:
      - stooj-dotfiles-workstation-common-i3-dunst-config-file-dir-managed

