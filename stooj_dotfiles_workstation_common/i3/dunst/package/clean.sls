# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- set tplsubname = tpldir.split('/')[1] %}
{%- set tplsubsubname = tpldir.split('/')[2] %}
{%- set tplsubsubroot = tplroot ~ '.' ~ tplsubname ~ '.' ~ tplsubsubname %}
{%- set sls_config_clean = tplsubsubroot ~ '.config.clean' %}

include:
  - {{ sls_config_clean }}

stooj-dotfiles-workstation-common-i3-dunst-package-install-pkg-removed:
  pkg.removed:
    - pkgs: {{ stooj_dotfiles_workstation_common.i3.dunst.pkgs | yaml }}
    - require:
      - sls: {{ sls_config_clean }}

