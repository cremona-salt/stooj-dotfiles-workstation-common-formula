# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}

stooj-dotfiles-workstation-common-i3-picom-config-clean-config-absent:
  file.absent:
    - name: /home/stooj/.config/picom/picom.conf

stooj-dotfiles-workstation-common-i3-picom-config-clean-dir-absent:
  file.absent:
    - name: /home/stooj/.config/picom
