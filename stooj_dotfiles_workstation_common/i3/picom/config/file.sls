# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set tplsubname = tpldir.split('/')[1] %}
{%- set tplsubsubname = tpldir.split('/')[2] %}
{%- set tplsubsubroot = tplroot ~ '.' ~ tplsubname ~ '.' ~ tplsubsubname %}
{%- set sls_package_install = tplsubsubroot ~ '.package.install' %}

include:
  - {{ sls_package_install }}

stooj-dotfiles-workstation-common-i3-picom-config-file-dir-managed:
  file.directory:
    - name: /home/stooj/.config/picom
    - user: stooj
    - group: stooj
    - mode: 700
    - makedirs: True

stooj-dotfiles-workstation-common-i3-picom-config-file-config-managed:
  file.managed:
    - name: /home/stooj/.config/picom/picom.conf
    - source: {{ files_switch(['picom/picom.conf.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-i3-picom-config-file-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        picom: {{ stooj_dotfiles_workstation_common.i3.picom | json }}
    - require:
      - stooj-dotfiles-workstation-common-i3-picom-config-file-dir-managed
