# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - .dunst
  - .feh
  - .i3wm
  - .picom
  - .rofi
  - .rofi_pass
  - .wallpaper
  - .xss_lock
