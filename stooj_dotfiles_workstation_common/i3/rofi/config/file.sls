# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set tplsubname = tpldir.split('/')[1] %}
{%- set tplsubsubname = tpldir.split('/')[2] %}
{%- set tplsubsubroot = tplroot ~ '.' ~ tplsubname ~ '.' ~ tplsubsubname %}
{%- set sls_package_install = tplsubsubroot ~ '.package.install' %}
{%- set sls_xorg_config = tplroot ~ '.xorg.config.file' %}

include:
  - {{ sls_package_install }}
  - {{ sls_xorg_config }}

stooj-dotfiles-workstation-common-i3-rofi-config-file-dir-managed:
  file.directory:
    - name: /home/stooj/.config/rofi
    - user: stooj
    - group: stooj
    - mode: 755
    - makedirs: True

{% for conffile in ['config', 'colours', 'emoji'] %}
stooj-dotfiles-workstation-common-i3-rofi-config-file-config-{{ conffile }}-managed:
  file.managed:
    - name: /home/stooj/.config/rofi/{{ conffile }}.rasi
    - source: {{ files_switch(['rofi/' ~ conffile ~ '.rasi.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-i3-rofi-config-file-config-' ~ conffile ~ '-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        rofi: {{ stooj_dotfiles_workstation_common.i3.rofi | json }}
        scaling_factor: {{ salt.grains.get('scaling_factor', 1 ) }}
    - require:
      - stooj-dotfiles-workstation-common-i3-rofi-config-file-dir-managed
{% endfor %}
