# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - .dunst.clean
  - .feh.clean
  - .i3wm.clean
  - .picom.clean
  - .rofi.clean
  - .rofi_pass.clean
  - .wallpaper.clean
  - .xss_lock.clean
