# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_workstation_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-workstation-common-ruby-config-file-ruby-gems-config-managed:
  file.managed:
    - name: /home/stooj/.config/zsh/env_includes/ruby.zsh
    - source: {{ files_switch(['ruby.zsh.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-ruby-config-file-ruby-gems-config-managed',
			      use_subpath=True
			     )
	      }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        ruby: {{ stooj_dotfiles_workstation_common.ruby | json }}
