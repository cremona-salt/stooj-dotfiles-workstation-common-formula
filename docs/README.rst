.. _readme:

stooj-dotfiles-workstation-common-formula
================

|img_travis| |img_sr|

.. |img_travis| image:: https://travis-ci.com/saltstack-formulas/stooj-dotfiles-workstation-common-formula.svg?branch=master
   :alt: Travis CI Build Status
   :scale: 100%
   :target: https://travis-ci.com/saltstack-formulas/stooj-dotfiles-workstation-common-formula
.. |img_sr| image:: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg
   :alt: Semantic Release
   :scale: 100%
   :target: https://github.com/semantic-release/semantic-release

A Saltstack formula to provision a workstation for stooj. It contains shared
states for work and personal workstations.

To separate concerns, these repos are arranged in a hierarchy. Each builds upon
the previous, and is arranged as a tree.

- stooj-dotfiles-common-formula           <- suitable for servers
  - stooj-dotfiles-workstation-formula    <- Shared config for work and personal
    - stooj-dotfiles-personal-workstation-formula     <- Personal desktops
    - stooj-dotfiles-work-workstation-formula         <- Work desktops


.. contents:: **Table of Contents**
   :depth: 1

General notes
-------------

If you need (non-default) configuration, please pay attention to the ``pillar.example`` file and/or `Special notes`_ section.


Available states
----------------

.. contents::
   :local:

``stooj_dotfiles_workstation_common``
^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

Install all stooj dotfiles appropriate for a desktop environment.


Testing
-------

Linux testing is done with ``kitchen-salt``.

Requirements
^^^^^^^^^^^^

* Ruby
* Docker

.. code-block:: bash

   $ gem install bundler
   $ bundle install
   $ bin/kitchen test [platform]

Where ``[platform]`` is the platform name defined in ``kitchen.yml``,
e.g. ``debian-9-2019-2-py3``.

``bin/kitchen converge``
^^^^^^^^^^^^^^^^^^^^^^^^

Creates the docker instance and runs the ``stooj_dotfiles_workstation_common`` main state, ready for testing.

``bin/kitchen verify``
^^^^^^^^^^^^^^^^^^^^^^

Runs the ``inspec`` tests on the actual instance.

``bin/kitchen destroy``
^^^^^^^^^^^^^^^^^^^^^^^

Removes the docker instance.

``bin/kitchen test``
^^^^^^^^^^^^^^^^^^^^

Runs all of the stages above in one go: i.e. ``destroy`` + ``converge`` + ``verify`` + ``destroy``.

``bin/kitchen login``
^^^^^^^^^^^^^^^^^^^^^

Gives you SSH access to the instance for manual testing.

