# -*- coding: utf-8 -*-
# vim: ft=yaml
---
# libtofs.jinja must work with tofs.files_switch looked up list
users:
  groups:
    scard:
      state: present
    stooj:
      state: present
  stooj:
    password: $6$Ki1SJKAnzl4TU8nE$ST4bWTmEw.FtLJ.XR7.98CW9wqKpwNFObvL5QIwyTtc/ZPRKfzchd/5K03t/7tdPSEGp3Wsa24ftwwwXT12qL0
    groups:
      - stooj
      - scard
      - users
