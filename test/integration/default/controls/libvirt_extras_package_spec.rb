# frozen_string_literal: true

packages = [
    'ebtables',
    'dnsmasq',
    'virt-manager',
]
packages.each do |pkg| 
  control 'stooj_dotfiles_workstation_common libvirt extras' + pkg + ' package' do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
