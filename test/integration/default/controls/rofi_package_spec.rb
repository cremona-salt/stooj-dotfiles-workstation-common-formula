# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common rofi package' do
  title 'should be installed'

  describe package('rofi') do
    it { should be_installed }
  end
end
