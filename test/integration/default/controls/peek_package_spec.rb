# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common peek package' do
  title 'should be installed'

  describe package('peek') do
    it { should be_installed }
  end
end
