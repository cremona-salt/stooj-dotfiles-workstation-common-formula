# frozen_string_literal: true
#
control 'stooj_dotfiles_workstation_common xdg user dirs config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/user-dirs.dirs') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('XDG_DESKTOP_DIR="$HOME/desktop"') }
    its('content') { should include('XDG_DOWNLOAD_DIR="$HOME/downloads"') }
    its('content') { should include('XDG_TEMPLATES_DIR="$HOME/templates"') }
    its('content') { should include('XDG_PUBLICSHARE_DIR="$HOME/public"') }
    its('content') { should include('XDG_DOCUMENTS_DIR="$HOME/documents"') }
    its('content') { should include('XDG_MUSIC_DIR="$HOME/music"') }
    its('content') { should include('XDG_PICTURES_DIR="$HOME/pictures"') }
    its('content') { should include('XDG_VIDEOS_DIR="$HOME/videos"') }
  end
end

control 'stooj_dotfiles_workstation_common xdg user dirs locale config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/user-dirs.locale') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('en_GB') }
  end
end
