# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common gdm service' do
  impact 0.5
  title 'should be running and enabled'

  describe service('gdm') do
    it { should be_enabled }
  end
end
