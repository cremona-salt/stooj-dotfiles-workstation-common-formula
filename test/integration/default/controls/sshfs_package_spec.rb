# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common sshfs package' do
  title 'should be installed'

  describe package('sshfs') do
    it { should be_installed }
  end
end
