# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common acpi package' do
  title 'should be installed'

  describe package('acpi') do
    it { should be_installed }
  end
end
