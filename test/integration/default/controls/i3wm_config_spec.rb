# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common i3wm config dir' do
  title 'should be created'

  describe directory('/home/stooj/.config/i3') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0755' }
  end
end


control 'stooj_dotfiles_workstation_common i3wm config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/i3/config') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# i3 config file (v4)') }
    its('content') { should include('bindsym XF86MonBrightnessDown exec xbacklight -dec 10') }
    its('content') { should include('exec --no-startup-id ~/.config/i3/autostart') }
  end
end

control 'stooj_dotfiles_workstation_common i3wm config autostart dir' do
  title 'should be created'

  describe directory('/home/stooj/.config/i3/autostart.d') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
  end
end

control 'stooj_dotfiles_workstation_common i3wm autostart file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/i3/autostart') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('#!/bin/sh') }
    its('content') { should include('for file in "$HOME"/.config/i3/autostart.d/*; do') }
  end
end

control 'stooj_dotfiles_workstation_common i3wm config script dir' do
  title 'should be created'

  describe directory('/home/stooj/.config/i3/scripts') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
  end
end

control 'stooj_dotfiles_workstation_common i3wm script exit_menu' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/i3/scripts/exit_menu') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('[ "$select" = "Cancel" ] && exit 0') }
  end
end
