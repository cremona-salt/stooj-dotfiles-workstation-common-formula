# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common xss-lock package' do
  title 'should be installed'

  describe package('xss-lock') do
    it { should be_installed }
  end
end
