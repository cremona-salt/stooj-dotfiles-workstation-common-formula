# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common taskwarrior package' do
  title 'should be installed'

  describe package('task') do
    it { should be_installed }
  end
end
