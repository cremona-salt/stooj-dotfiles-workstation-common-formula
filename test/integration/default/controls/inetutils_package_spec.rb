# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common inetutils package' do
  title 'should be installed'

  describe package('inetutils') do
    it { should be_installed }
  end
end
