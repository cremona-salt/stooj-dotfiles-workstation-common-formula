# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common gdm package' do
  title 'should be installed'

  describe package('gdm') do
    it { should be_installed }
  end
end
