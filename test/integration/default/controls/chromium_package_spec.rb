# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common chromium package' do
  title 'should be installed'

  describe package('chromium') do
    it { should be_installed }
  end
end
