# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common xorg-xrandr package' do
  title 'should be installed'

  describe package('xorg-xrandr') do
    it { should be_installed }
  end
end
