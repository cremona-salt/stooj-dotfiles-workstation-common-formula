# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common gnu-free-fonts package' do
  title 'should be installed'

  describe package('gnu-free-fonts') do
    it { should be_installed }
  end
end

control 'stooj_dotfiles_workstation_common noto-fonts-emoji package' do
  title 'should be installed'

  describe package('noto-fonts-emoji') do
    it { should be_installed }
  end
end

control 'stooj_dotfiles_workstation_common ttf-fira-code package' do
  title 'should be installed'

  describe package('ttf-fira-code') do
    it { should be_installed }
  end
end

control 'stooj_dotfiles_workstation_common adobe-source-code-pro-fonts package' do
  title 'should be installed'

  describe package('adobe-source-code-pro-fonts') do
    it { should be_installed }
  end
end

control 'stooj_dotfiles_workstation_common noto-fonts-cjk package' do
  title 'should be installed'

  describe package('noto-fonts-cjk') do
    it { should be_installed }
  end
end
