# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common youtube_dl package' do
  title 'should be installed'

  describe package('youtube-dl') do
    it { should be_installed }
  end
end
