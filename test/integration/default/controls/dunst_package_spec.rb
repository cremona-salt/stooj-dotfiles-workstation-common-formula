# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common dunst package' do
  title 'should be installed'

  describe package('dunst') do
    it { should be_installed }
  end
end
