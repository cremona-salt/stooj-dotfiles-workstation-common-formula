# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common librecad package' do
  title 'should be installed'

  describe package('librecad') do
    it { should be_installed }
  end
end
