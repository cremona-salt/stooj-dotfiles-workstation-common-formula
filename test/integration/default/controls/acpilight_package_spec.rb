# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common acpilight package' do
  title 'should be installed'

  describe package('acpilight') do
    it { should be_installed }
  end
end
