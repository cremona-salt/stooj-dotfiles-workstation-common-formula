# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common xdg package' do
  title 'should be installed'

  describe package('xdg-user-dirs') do
    it { should be_installed }
  end
end
