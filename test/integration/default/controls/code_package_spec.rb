# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common code utilities highlight package' do
  title 'should be installed'

  describe package('highlight') do
    it { should be_installed }
  end
end

