# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common tmate package' do
  title 'should be installed'

  describe package('tmate') do
    it { should be_installed }
  end
end
