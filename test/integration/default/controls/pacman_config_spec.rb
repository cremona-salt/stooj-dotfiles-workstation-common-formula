# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common pacman config file' do
  title 'should match desired lines'

  describe file('/etc/pacman.conf') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('[multilib]') }
  end
end
