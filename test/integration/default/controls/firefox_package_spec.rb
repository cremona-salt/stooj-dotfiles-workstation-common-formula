# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common firefox package' do
  title 'should be installed'

  describe package('firefox') do
    it { should be_installed }
  end
end

profiles = ['default']
profiles.each do |profile|
  control 'stooj_dotfiles_workstation_common firefox ' + profile + ' launch script' do
    title 'should match desired lines'
  
    describe file('/home/stooj/bin/' + profile + '-firefox') do
      it { should be_file }
      it { should be_owned_by 'stooj' }
      it { should be_grouped_into 'stooj' }
      its('mode') { should cmp '0700' }
      its('content') { should include('# Your changes will be overwritten.') }
      its('content') { should include('firefox -P ' + profile) }
    end
  end
end
