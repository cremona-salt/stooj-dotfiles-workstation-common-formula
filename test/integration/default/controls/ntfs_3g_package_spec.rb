# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common ntfs_3g package' do
  title 'should be installed'

  describe package('ntfs-3g') do
    it { should be_installed }
  end
end
