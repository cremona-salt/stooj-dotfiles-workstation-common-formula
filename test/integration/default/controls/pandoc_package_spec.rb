# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common pandoc package' do
  title 'should be installed'

  describe package('pandoc') do
    it { should be_installed }
  end
end
