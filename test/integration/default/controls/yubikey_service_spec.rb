# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common yubikey service' do
  impact 0.5
  title 'should be running and enabled'

  describe service('pcscd.socket') do
    it { should be_enabled }
    it { should be_running }
  end
end

