# frozen_string_literal: true
#
control 'stooj_dotfiles_workstation_common tmate config dir' do
  title 'should be created'

  describe directory('/home/stooj/.config/tmate') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
  end
end

control 'stooj_dotfiles_workstation_common tmate config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/tmate/tmate.conf') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# Set the prefix to ^s') }
    its('content') { should include('# Split windows with better keys, keeping cwd') }
  end
end
