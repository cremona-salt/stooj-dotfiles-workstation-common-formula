# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common mpv package' do
  title 'should be installed'

  describe package('mpv') do
    it { should be_installed }
  end
end
