# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common qutebrowser package' do
  title 'should be installed'

  describe package('qutebrowser') do
    it { should be_installed }
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser launch script' do
  title 'should match desired lines'

  describe file('/home/stooj/bin/qutebrowser') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# From https://github.com/ayekat/dotfiles/blob/master/bin/qutebrowser') }
  end
end

