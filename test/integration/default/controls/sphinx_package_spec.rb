# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common sphinx package' do
  title 'should be installed'

  describe package('python-sphinx') do
    it { should be_installed }
  end
end
