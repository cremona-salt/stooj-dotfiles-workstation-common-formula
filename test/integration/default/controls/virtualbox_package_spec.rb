# frozen_string_literal: true

packages = [
    'virtualbox',
    'virtualbox-host-modules',
    'virtualbox-guest-iso',
]
packages.each do |pkg| 
  control 'stooj_dotfiles_workstation_common virtualbox ' + pkg + ' package' do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end

