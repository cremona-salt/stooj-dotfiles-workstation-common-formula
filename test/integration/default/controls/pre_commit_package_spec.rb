# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common pre_commit package' do
  title 'should be installed'

  describe package('python-pre-commit') do
    it { should be_installed }
  end
end

