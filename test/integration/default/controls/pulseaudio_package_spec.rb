# frozen_string_literal: true

packages = ['pulseaudio', 'pamixer', 'pavucontrol', 'pulsemixer']
packages.each do |pkg| 
  control 'stooj_dotfiles_workstation_common pulseaudio ' + pkg + ' package' do
    title 'should be installed'
  
    describe package(pkg) do
      it { should be_installed }
    end
  end
end
