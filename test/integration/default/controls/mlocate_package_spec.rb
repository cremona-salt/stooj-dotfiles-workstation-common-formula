# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common mlocate package' do
  title 'should be installed'

  describe package('mlocate') do
    it { should be_installed }
  end
end
