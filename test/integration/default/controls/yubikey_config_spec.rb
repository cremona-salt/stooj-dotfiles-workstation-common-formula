# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common yubikey udev rule' do
  title 'should match desired lines'

  describe file('/etc/udev/rules.d/71-gnupg-ccid.rules') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('ACTION=="add", SUBSYSTEM=="usb", ') }
  end
end

control 'stooj_dotfiles_workstation_common yubikey trusted key' do
  title 'should exist in keychain'

  gpg_status = command('sudo -u stooj gpg --list-keys | grep CD65C4E0')

  describe gpg_status do
    its('exit_status') { should eq 0 }
    its('stdout') { should include('0EDB1055143A37C1F68263F77D60C8C7CD65C4E0') }
  end
end
