# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common shellcheck package' do
  title 'should be installed'

  describe package('shellcheck') do
    it { should be_installed }
  end
end
