# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common neovim-qt package' do
  title 'should be installed'

  describe package('neovim-qt') do
    it { should be_installed }
  end
end
