# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common picom package' do
  title 'should be installed'

  describe package('picom') do
    it { should be_installed }
  end
end
