# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common doctl package' do
  title 'should be installed'

  describe package('doctl') do
    it { should be_installed }
  end
end
