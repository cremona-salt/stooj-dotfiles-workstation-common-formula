# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common qcad package' do
  title 'should be installed'

  describe package('qcad') do
    it { should be_installed }
  end
end
