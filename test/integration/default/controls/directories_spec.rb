# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common directory src created' do
  title 'should be created'

  describe directory('/home/stooj/src') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
  end
end
