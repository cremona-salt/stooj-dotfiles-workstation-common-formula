# frozen_string_literal: true

packages = ['pcsclite', 'ccid', 'libusb-compat', 'usbutils']

packages.each do |pkg|
  control 'stooj_dotfiles_workstation_common yubikey ' + pkg + ' package' do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end

