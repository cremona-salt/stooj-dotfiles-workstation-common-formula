# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common timewarrior repo' do
  title 'should be cloned'

  describe directory('/home/stooj/code/time') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
  end
end

control 'stooj_dotfiles_workstation_common timewarrior env zsh file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/env_includes/timewarrior.zsh') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('TIMEWARRIORDB=/home/stooj/code/time') }
    its('content') { should include('export TIMEWARRIORDB') }
  end
end
