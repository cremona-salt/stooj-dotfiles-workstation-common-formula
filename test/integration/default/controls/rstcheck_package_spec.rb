# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common rstcheck package' do
  title 'should be installed'

  describe package('rstcheck') do
    it { should be_installed }
  end
end
