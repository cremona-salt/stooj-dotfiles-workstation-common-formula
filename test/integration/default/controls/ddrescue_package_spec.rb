# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common ddrescue package' do
  title 'should be installed'

  describe package('ddrescue') do
    it { should be_installed }
  end
end
