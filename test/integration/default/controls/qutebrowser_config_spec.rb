# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common qutebrowser config dir' do
  title 'should exist'

  describe directory('/home/stooj/.config/qutebrowser') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser config bookmarks dir' do
  title 'should exist'

  describe directory('/home/stooj/.config/qutebrowser/bookmarks') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser bookmarks file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/qutebrowser/bookmarks/urls') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('https://regex101.com/ Online regex tester and debugger: PHP, PCRE, Python, Golang and JavaScript') }
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/qutebrowser/config.py') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# #   qute://help/configuring.html') }
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser keys file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/qutebrowser/keys.conf') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include("# In this config file, qutebrowser's key bindings are configured.") }
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser qutebrowser.conf file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/qutebrowser/qutebrowser.conf') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# Configfile for qutebrowser.') }
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser scripts repo' do
  title 'should exist'

  describe directory('/home/stooj/code/qutebrowser-scripts') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser scripts entrypoint dir' do
  title 'should exist'

  describe directory('/home/stooj/.local/share/qutebrowser') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser userscripts dir' do
  title 'should exist'

  describe directory('/home/stooj/.local/share/qutebrowser/userscripts') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
  end
end

scripts = ['markdownlink', 'plainlink', 'rstlink']
scripts.each do |script|
  control 'stooj_dotfiles_workstation_common qutebrowser script ' + script do
    title 'should exist'

    describe file('/home/stooj/.local/share/qutebrowser/userscripts/' + script) do
      it { should be_file }
      it { should be_owned_by 'stooj' }
      it { should be_grouped_into 'stooj' }
      its('content') { should include('# Your changes will be overwritten.') }
      its('content') { should include('/usr/bin/python "/home/stooj/code/qutebrowser-scripts/') }
    end
  end
end

control 'stooj_dotfiles_workstation_common config file qutescript repo installed' do
  title 'should exist'

  describe file('/home/stooj/.local/lib/python3.8/site-packages/qutescript/__init__.py') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('content') { should include('from .request import Request, build_request') }
  end
end

profiles = ['default']
profiles.each do |profile|
  control 'stooj_dotfiles_workstation_common qutebrowser ' + profile + ' launch script' do
    title 'should exist'

    describe file('/home/stooj/bin/' + profile + '-qutebrowser') do
      it { should be_file }
      it { should be_owned_by 'stooj' }
      it { should be_grouped_into 'stooj' }
      its('content') { should include('# Your changes will be overwritten.') }
      its('content') { should include('$QUTEBROWSER -r ' + profile) }
    end
  end
end
