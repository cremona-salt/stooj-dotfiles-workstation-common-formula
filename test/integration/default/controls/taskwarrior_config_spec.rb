# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common taskwarrior repo' do
  title 'should be cloned'

  describe directory('/home/stooj/code/task') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
  end
end

control 'stooj_dotfiles_workstation_common taskwarrior config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/task/kitchen-taskrc') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('content') { should include('data.location=/home/stooj/code/task') }
  end
end

control 'stooj_dotfiles_workstation_common taskwarrior alias file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/rc_includes/kitchen-taskwarrior-alias.zsh') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('alias kitchen-taskwarrior=\'TASKRC="/home/stooj/.config/task/kitchen-taskrc" task\'') }
  end
end
