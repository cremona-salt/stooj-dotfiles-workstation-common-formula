# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common python gnupg package' do
  title 'should be installed'

  describe package('python-gnupg') do
    it { should be_installed }
  end
end
