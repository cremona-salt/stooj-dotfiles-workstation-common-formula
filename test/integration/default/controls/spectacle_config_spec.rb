# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common spectacle config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/spectaclerc') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('[Save]') }
    its('content') { should include('saveFilenameFormat = %Y-%M-%D-%H%m%S_%T_%d') }
  end
end
