# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common msmtp config dir' do
  title 'should be created'

  describe directory('/home/stooj/.config/msmtp') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
  end
end

control 'stooj_dotfiles_workstation_common msmtp config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/msmtp/config') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('defaults') }
    its('content') { should include('auth on') }
    its('content') { should include('account gmail') }
  end
end
