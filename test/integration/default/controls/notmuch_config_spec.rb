# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common notmuch config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.notmuch-config') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('name=stoo johnston') }
    its('content') { should include('path=/home/stooj/.local/share/mail') }
    its('content') { should include('primary_email=example@gmail.com') }
    its('content') { should include('other_email=example@hotmail.com;example@yahoo.com')
  end
end
