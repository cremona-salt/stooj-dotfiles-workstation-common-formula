# frozen_string_literal: true

packages = [
  'jre-openjdk',
  'jre8-openjdk',
]
packages.each do |pkg|
  control 'stooj_dotfiles_workstation_common ' + pkg + ' package' do
    title 'should be installed'
  
    describe package(pkg) do
      it { should be_installed }
    end
  end
end
