# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common timewarrior package' do
  title 'should be installed'

  describe package('timew') do
    it { should be_installed }
  end
end
