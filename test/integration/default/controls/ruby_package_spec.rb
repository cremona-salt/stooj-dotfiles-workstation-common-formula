# frozen_string_literal: true

packages = [
  'ruby',
  'ruby-irb'
]

packages.each do |pkg|
  control 'stooj_dotfiles_workstation_common ruby ' + pkg + ' package' do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
