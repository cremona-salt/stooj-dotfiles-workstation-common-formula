# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common signal package' do
  title 'should be installed'

  describe package('signal-desktop') do
    it { should be_installed }
  end
end
