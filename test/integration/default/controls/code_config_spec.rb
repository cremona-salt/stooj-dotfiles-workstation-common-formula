# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common code dir' do
  title 'should be created'

  describe directory('/home/stooj/code') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0755' }
  end
end

control 'stooj_dotfiles_workstation_common code salt dir' do
  title 'should be created'

  describe directory('/home/stooj/code/salt') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0755' }
  end
end

control 'stooj_dotfiles_workstation_common code docs dir' do
  title 'should be created'

  describe directory('/home/stooj/code/docs') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0755' }
  end
end

control 'stooj_dotfiles_workstation_common code salt template-formula' do
  title 'should be cloned'

  describe file('/home/stooj/code/salt/template-formula') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0755' }
  end
end

control 'stooj_dotfiles_workstation_common code salt template-formula readme content' do
  title 'should match desired lines'

  describe file('/home/stooj/code/salt/template-formula/FORMULA') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('name: TEMPLATE') }
  end
end

control 'stooj_dotfiles_workstation_common code salt template-formula remote' do
  title 'should be added'
  describe file('/home/stooj/code/salt/template-formula/.git/config') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('remote "gitlab"') }
    its('content') { should include('git@gitlab.com:stooj/template-formula.git') }
  end
end
