# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common element-desktop package' do
  title 'should be installed'

  describe package('element-desktop') do
    it { should be_installed }
  end
end
