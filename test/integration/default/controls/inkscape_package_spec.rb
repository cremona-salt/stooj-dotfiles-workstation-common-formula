# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common inkscape package' do
  title 'should be installed'

  describe package('inkscape') do
    it { should be_installed }
  end
end
