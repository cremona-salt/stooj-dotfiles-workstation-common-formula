# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common offlineimap config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.offlineimaprc') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('[general]') }
    its('content') { should include('metadata = ~/.local/share/offlineimap') }
    its('content') { should include('[Account gmail]') }
    its('content') { should include('localfolders = ~/.local/share/mail/gmail') }
    its('content') { should include('type = Gmail') }
  end
end
