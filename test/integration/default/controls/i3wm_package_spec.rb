# frozen_string_literal: true

packages = ['i3-gaps', 'i3blocks', 'i3lock', 'i3status']

packages.each do |pkg|
  control 'stooj_dotfiles_workstation_common i3 ' + pkg + ' package' do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
