# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common man_db package' do
  title 'should be installed'

  describe package('man-db') do
    it { should be_installed }
  end
end
