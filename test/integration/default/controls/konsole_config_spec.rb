# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common konsole config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/konsolerc') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('[Desktop Entry]') }
    its('content') { should include('DefaultProfile = stoos-profile.profile') }
    its('content') { should include('[KonsoleWindow]') }
    its('content') { should include('ShowMenuBarByDefault = False') }
  end
end

control 'stooj_dotfiles_workstation_common konsole stoos profile file' do
  title 'should match desired lines'

  describe file('/home/stooj/.local/share/konsole/stoos-profile.profile') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('[Appearance]') }
    its('content') { should include('ColorScheme=Gruvbox_dark') }
  end
end

control 'stooj_dotfiles_workstation_common konsole gruvbox colorscheme file' do
  title 'should match desired lines'

  describe file('/home/stooj/.local/share/konsole/Gruvbox_dark.colorscheme') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('[Background]') }
    its('content') { should include('Color=40,40,40') }
  end
end

control 'stooj_dotfiles_workstation_common konsole zsh config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/env_includes/konsole.zsh') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('TERMINAL=$(which konsole)') }
    its('content') { should include('export TERMINAL') }
  end
