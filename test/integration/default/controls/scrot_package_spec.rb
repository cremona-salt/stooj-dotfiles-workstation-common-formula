# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common scrot package' do
  title 'should be installed'

  describe package('scrot') do
    it { should be_installed }
  end
end
