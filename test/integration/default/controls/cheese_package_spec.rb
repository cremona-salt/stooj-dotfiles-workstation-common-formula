# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common cheese package' do
  title 'should be installed'

  describe package('cheese') do
    it { should be_installed }
  end
end
