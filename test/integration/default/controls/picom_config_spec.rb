# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common picom config dir' do
  title 'should be created'

  describe directory('/home/stooj/.config/picom') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
  end
end


control 'stooj_dotfiles_workstation_common picom config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/picom/picom.conf') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
  end
end
