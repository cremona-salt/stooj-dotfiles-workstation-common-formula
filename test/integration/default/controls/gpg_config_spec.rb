# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common gpg config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.gnupg/gpg.conf') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('default-key') }
  end
end

control 'stooj_dotfiles_workstation_common gpg agent config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.gnupg/gpg-agent.conf') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('pinentry-program /usr/bin/pinentry-qt') }
  end
end
