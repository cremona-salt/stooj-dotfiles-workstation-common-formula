# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common w3m config dir' do
  title 'should be created'

  describe directory('/home/stooj/.w3m') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
  end
end

control 'stooj_dotfiles_workstation_common w3m config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.w3m/config') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
  end
end
