# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common spectacle package' do
  title 'should be installed'

  describe package('spectacle') do
    it { should be_installed }
  end
end
