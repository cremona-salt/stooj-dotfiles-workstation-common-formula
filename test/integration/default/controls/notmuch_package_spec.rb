# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common notmuch package' do
  title 'should be installed'

  describe package('notmuch') do
    it { should be_installed }
  end
end
