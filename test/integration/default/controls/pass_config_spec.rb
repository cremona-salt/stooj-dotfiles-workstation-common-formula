# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common pass repo' do
  title 'should be cloned'

  describe directory('/home/stooj/.kitchen-pass-store') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
  end
end

control 'stooj_dotfiles_workstation_common pass config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.kitchen-pass-store/FORMULA') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('content') { should include('name: TEMPLATE') }
  end
end

control 'stooj_dotfiles_workstation_common pass alias file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/rc_includes/kitchen-pass-alias.zsh') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('alias kitchen-pass=\'PASSWORD_STORE_DIR="/home/stooj/.kitchen-pass-store" pass\'') }
  end
end
