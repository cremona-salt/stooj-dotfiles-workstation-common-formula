# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common rofi-pass config dir' do
  title 'should be created'

  describe directory('/home/stooj/.config/rofi-pass') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0755' }
  end
end

control 'stooj_dotfiles_workstation_common rofi-pass config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/rofi-pass/config') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('EDITOR=\'nvim-qt\'') }
  end
end
