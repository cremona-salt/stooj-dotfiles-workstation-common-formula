# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common service dir' do
  title 'should be created'
  describe directory('/home/stooj/.config/systemd/user') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0755' }
  end
end

control 'stooj_dotfiles_workstation_common wallpaper service unit file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/systemd/user/change_i3_background.service') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('Description=Change i3 wallpaper') }
  end
end

control 'stooj_dotfiles_workstation_common wallpaper timer unit file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/systemd/user/change_i3_background.timer') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('Description=Change i3 wallpaper every minute') }
  end
end

control 'stooj_dotfiles_workstation_common wallpaper bin script' do
  title 'should match desired lines'

  describe file('/home/stooj/bin/change_wallpaper') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('import datetime') }
  end
end

control 'stooj_dotfiles_workstation_common wallpaper symlink' do
  title 'should be in correct state'

  describe file('/home/stooj/.config/desktop-backgrounds') do
    it { should be_symlink }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('link_path') { should eq '/tmp' }
  end
end
