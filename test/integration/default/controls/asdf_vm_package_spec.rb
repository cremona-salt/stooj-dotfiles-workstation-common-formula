# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common asdf_vm repo installed' do
  title 'should exist'

  describe file('/home/stooj/.local/share/asdf/ballad-of-asdf.md') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('content') { should include('*This was the mail I wrote to a few friends') }
  end
end


