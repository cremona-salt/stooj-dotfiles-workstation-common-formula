# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common gdm accountsservice users config dir' do
  title 'should be created'

  describe directory('/var/lib/AccountsService/users/') do
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0700' }
  end
end


control 'stooj_dotfiles_workstation_common gdm config file' do
  title 'should match desired lines'

  describe file('/var/lib/AccountsService/users/stooj') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('XSession=i3') }
    its('content') { should include('Email=jmiller@ceres.station') }
  end
end



