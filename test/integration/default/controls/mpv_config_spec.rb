# frozen_string_literal: true
#
control 'stooj_dotfiles_workstation_common mpv config dir' do
  title 'should be created'

  describe directory('/home/stooj/.config/mpv') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
  end
end

control 'stooj_dotfiles_workstation_common mpv config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/mpv/mpv.conf') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('audio-channels=auto-safe') }
    its('content') { should include('stop-screensaver = "yes"') }
  end
end
