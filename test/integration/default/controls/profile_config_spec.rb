# frozen_string_literal: true
#
control 'stooj_dotfiles_workstation_common profile config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.profile') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('ZDOTDIR=/home/stooj/.config/zsh') }
    its('content') { should include('. $ZDOTDIR/.zshenv') }
  end
end
