# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common msmtp package' do
  title 'should be installed'

  describe package('msmtp') do
    it { should be_installed }
  end
end

control 'stooj_dotfiles_workstation_common msmtp-mta package' do
  title 'should be installed'

  describe package('msmtp-mta') do
    it { should be_installed }
  end
end
