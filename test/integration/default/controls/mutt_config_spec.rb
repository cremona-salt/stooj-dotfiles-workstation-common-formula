# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common mutt rc config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/mutt/muttrc') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('set folder = ~/.local/share/mail') }
    its('content') { should include('set spoolfile = +"gmail/inbox"') }
    its('content') { should include('folder-hook +gmail/.* "source ~/.config/mutt/gmail"') }
    its('content') { should include('folder-hook +gmail/django-users/.* "source ~/.config/mutt/django-users"') }
    its('content') { should include('alternates example@gmail.com') }
  end
end

control 'stooj_dotfiles_workstation_common mutt account config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/mutt/gmail') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('unset record') }
    its('content') { should include('set sendmail="/usr/bin/msmtp -a gmail"') }
    its('content') { should include('set from="example@gmail.com"') }
  end
end

control 'stooj_dotfiles_workstation_common mutt common config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/mutt/common') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('set check_new = yes') }
    its('content') { should include('set mbox_type=Maildir') }
    its('content') { should include('set sort = \'threads\'') }
    its('content') { should include('my_hdr X-Clacks-Overhead: GNU Terry Pratchett') }
  end
end

control 'stooj_dotfiles_workstation_common mutt macros config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/mutt/macros') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('macro index \cn     <next-unread-mailbox>') }
    its('content') { should include('macro pager j       <next-line>') }
    its('content') { should include('macro pager k       <previous-line>') }
  end
end

control 'stooj_dotfiles_workstation_common mutt trigger config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/mutt/django-users') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('set record = +gmail/sent') }
    its('content') { should include('set from="example+django-users@gmail.com') }
  end
end

control 'stooj_dotfiles_workstation_common mutt colours config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/mutt/colours') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('color normal     color188 color237') }
  end
end

control 'stooj_dotfiles_workstation_common mutt mailcap config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/mutt/mailcap') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('text/html; w3m -v -F -T text/html %s;') }
    its('content') { should include('image/*; feh %s') }
  end
end

control 'stooj_dotfiles_workstation_common mutt pgp config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/mutt/pgp') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('set pgp_autosign=yes') }
    its('content') { should include('set pgp_sign_as=0xCD65C4E0') }
  end
end

control 'stooj_dotfiles_workstation_common neomutt alias file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/rc_includes/neomutt-alias.zsh') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('alias mutt=\'neomutt\'') }
  end
end
