# frozen_string_literal: true

packages = [
    'libdvdread',
    'libdvdcss',
    'libdvdnav'
]
packages.each do |pkg| 
  control 'stooj_dotfiles_workstation_common dvd playback ' + pkg + ' package' do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
