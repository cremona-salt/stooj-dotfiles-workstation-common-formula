# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common asdf_vm env zsh file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/env_includes/asdf_vm.zsh') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('ASDF_DATA_DIR="$HOME"/.local/share/asdf') }
    its('content') { should include('export ASDF_DATA_DIR') }
  end
end

control 'stooj_dotfiles_workstation_common asdf_vm rc zsh file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/rc_includes/asdf_vm.zsh') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('source "$HOME"/.local/share/asdf/asdf.sh') }
  end
end

control 'stooj_dotfiles_workstation_common asdf_vm base tool versions file' do
  title 'should match desired lines'
  describe file('/home/stooj/.tool-versions') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('ruby 2.7.1') }
  end
end
