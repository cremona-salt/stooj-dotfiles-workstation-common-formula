# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common nmap package' do
  title 'should be installed'

  describe package('nmap') do
    it { should be_installed }
  end
end
