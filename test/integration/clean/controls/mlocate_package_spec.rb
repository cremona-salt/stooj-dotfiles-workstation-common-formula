# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common mlocate package' do
  title 'should not be installed'

  describe package('mlocate') do
    it { should_not be_installed }
  end
end
