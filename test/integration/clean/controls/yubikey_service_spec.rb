# frozen_string_literal: true
control 'stooj_dotfiles_workstation_common yubikey service' do
  impact 0.5
  title 'should not be enabled'

  describe service('pcscd.socket') do
    it { should_not be_enabled }
    it { should_not be_running }
  end
end


