# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common directory src removed' do
  title 'the src dir should not exist'

  describe directory('/home/stooj/src') do
    it { should_not exist }
  end
end
