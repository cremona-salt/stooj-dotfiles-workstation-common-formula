# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common rofi-pass config file' do
  title 'should not exist'

  describe file('/home/stooj/.config/rofi-pass/config') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common rofi-pass config dir' do
  title 'should not exist'

  describe directory('/home/stooj/.config/rofi-pass') do
    it { should_not exist }
  end
end
