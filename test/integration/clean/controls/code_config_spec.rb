# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common code dir' do
  title 'should not exist'

  describe directory('/home/stooj/code') do
    it { should_not exist }
  end
end
