# frozen_string_literal: true
#
control 'stooj_dotfiles_workstation_common golang zsh file' do
  title 'should not exist'

  describe file('/home/stooj/.config/zsh/env_includes/golang.zsh') do
    it { should_not exist }
  end
end
