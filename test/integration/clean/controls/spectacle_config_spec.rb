# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common spectacle config file' do
  title 'should not exist'

  describe file('/home/stooj/.config/spectaclerc') do
    it { should_not exist }
  end
end
