# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common w3m config file' do
  title 'should not exist'

  describe file('/home/stooj/.w3m/config') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common w3m config dir' do
  title 'should not exist'

  describe directory('/home/stooj/.w3m') do
    it { should_not exist }
  end
end
