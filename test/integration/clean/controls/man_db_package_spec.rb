# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common man_db package' do
  title 'should not be installed'

  describe package('man-db') do
    it { should_not be_installed }
  end
end
