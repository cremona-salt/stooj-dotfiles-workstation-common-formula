# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common yubikey udev rule' do
  title 'should not exist'

  describe file('/etc/udev/rules.d/71-gnupg-ccid.rules') do
    it { should_not exist }
  end
end
