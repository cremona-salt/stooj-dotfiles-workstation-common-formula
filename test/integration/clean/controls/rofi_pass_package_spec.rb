# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common rofi-pass package' do
  title 'should not be installed'

  describe package('rofi-pass') do
    it { should_not be_installed }
  end
end
