# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common gdm accountsservice users config dir' do
  title 'should not exist'

  describe directory('/var/lib/AccountsService/users/') do
    it { should_not exist }
  end
end


control 'stooj_dotfiles_workstation_common gdm config file' do
  title 'should not exist'

  describe file('/var/lib/AccountsService/users/stooj') do
    it { should_not exist }
  end
end




