# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common signal package' do
  title 'should not be installed'

  describe package('signal-desktop') do
    it { should_not be_installed }
  end
end
