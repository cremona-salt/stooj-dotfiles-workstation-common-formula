# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common inetutils package' do
  title 'should not be installed'

  describe package('inetutils') do
    it { should_not be_installed }
  end
end
