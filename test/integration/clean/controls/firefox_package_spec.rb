# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common firefox package' do
  title 'should not be installed'

  describe package('firefox') do
    it { should_not be_installed }
  end
end

profiles = ['default']
profiles.each do |profile|
  control 'stooj_dotfiles_workstation_common firefox ' + profile + ' launch script' do
    title 'should not exist'
  
    describe file('/home/stooj/bin/' + profile + '-firefox') do
      it { should_not exist }
    end
  end
end
