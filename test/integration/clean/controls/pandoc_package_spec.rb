# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common pandoc package' do
  title 'should not be installed'

  describe package('pandoc') do
    it { should_not be_installed }
  end
end
