# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common timewarrior package' do
  title 'should not be installed'

  describe package('timew') do
    it { should_not be_installed }
  end
end
