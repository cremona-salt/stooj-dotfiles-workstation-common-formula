# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common inkscape package' do
  title 'should not be installed'

  describe package('inkscape') do
    it { should_not be_installed }
  end
end
