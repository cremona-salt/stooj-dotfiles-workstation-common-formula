# frozen_string_literal: true
#
control 'stooj_dotfiles_workstation_common i3wm script exit_menu' do
  title 'should not exist'

  describe file('/home/stooj/.config/i3/scripts/exit_menu') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common i3wm config script dir' do
  title 'should not exist'

  describe directory('/home/stooj/.config/i3/scripts') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common i3wm config autostart dir' do
  title 'should not exist'

  describe directory('/home/stooj/.config/i3/autostart.d') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common i3wm autostart file' do
  title 'should not exist'

  describe file('/home/stooj/.config/i3/autostart') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common i3wm config file' do
  title 'should not exist'

  describe file('/home/stooj/.config/i3/config') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common i3wm config dir' do
  title 'should not exist'

  describe directory('/home/stooj/.config/i3') do
    it { should_not exist }
  end
end
