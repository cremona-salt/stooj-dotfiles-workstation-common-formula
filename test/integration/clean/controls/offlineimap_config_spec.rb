# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common offlineimap config file' do
  title 'should not exist'

  describe file('/home/stooj/.config/offlineimap/config') do
    it { should_not exist }
  end
end
