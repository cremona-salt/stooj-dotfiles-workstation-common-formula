# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common mutt pgp config file' do
  title 'should not exist'
  describe file('/home/stooj/.config/mutt/pgp') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common mutt mailcap config file' do
  title 'should not exist'

  describe file('/home/stooj/.config/mutt/mailcap') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common mutt colours config file' do
  title 'should not exist'

  describe file('/home/stooj/.config/mutt/colours') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common mutt trigger config file' do
  title 'should not exist'

  describe file('/home/stooj/.config/mutt/django-users') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common mutt macros config file' do
  title 'should not exist'

  describe file('/home/stooj/.config/mutt/macros') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common mutt common config file' do
  title 'should not exist'

  describe file('/home/stooj/.config/mutt/common') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common mutt account config file' do
  title 'should not exist'

  describe file('/home/stooj/.config/mutt/gmail') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common mutt rc config file' do
  title 'should not exist'

  describe file('/home/stooj/.config/mutt/muttrc') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common neomutt alias file' do
  title 'should not exist'

  describe file('/home/stooj/.config/zsh/rc_includes/neomutt-alias.zsh') do
    it { should_not exist }
  end
end


