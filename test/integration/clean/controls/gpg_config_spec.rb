# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common gpg config file' do
  title 'should be absent'

  describe file('/home/stooj/.gnupg/gpg.conf') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common gpg agent config file' do
  title 'should be absent'

  describe file('/home/stooj/.gnupg/gpg-agent.conf') do
    it { should_not exist }
  end
end

