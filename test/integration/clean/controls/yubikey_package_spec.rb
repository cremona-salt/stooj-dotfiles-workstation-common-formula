# frozen_string_literal: true

packages = ['pcscd', 'ccid', 'libusb-compat', 'usbutils']

packages.each do |pkg|
  control 'stooj_dotfiles_workstation_common yubikey ' + pkg + ' package' do
    title 'should be not installed'

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
