# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common taskwarrior repo' do
  title 'should not exist'

  describe file('/home/stooj/code/task') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common taskwarrior alias file' do
  title 'should not exist'

  describe file('/home/stooj/.config/zsh/rc_includes/kitchen-taskwarrior-alias.zsh') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common taskwarrior config file' do
    title 'should not exist'

    describe file('/home/stooj/.config/task/kitchen-taskwarrior-taskrc') do
    it { should_not exist }
  end
end
