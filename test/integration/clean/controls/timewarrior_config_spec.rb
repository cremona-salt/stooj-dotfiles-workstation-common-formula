# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common timewarrior repo' do
  title 'should not exist'

  describe file('/home/stooj/code/time') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common timewarrior env zsh file' do
  title 'should not exist'

  describe file('/home/stooj/.config/zsh/env_includes/timewarrior.zsh') do
    it { should_not exist }
  end
end
