# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common wallpaper service unit file' do
  title 'should not exist'

  describe file('/home/stooj/.config/systemd/user/change_i3_background.service') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common wallpaper timer unit file' do
  title 'should not exist'

  describe file('/home/stooj/.config/systemd/user/change_i3_background.timer') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common wallpaper bin script' do
  title 'should not exist'

  describe file('/home/stooj/bin/change_wallpaper') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common wallpaper symlink' do
  title 'should not exist'

  describe file('/home/stooj/.config/desktop-backgrounds') do
    it { should_not exist }
  end
end
