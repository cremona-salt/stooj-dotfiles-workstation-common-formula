# frozen_string_literal: true
#
control 'stooj_dotfiles_workstation_common xdg user dirs config file' do
  title 'should be absent'

  describe file('/home/stooj/.config/user-dirs.dirs') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common xdg user dirs locale config file' do
  title 'should be absent'

  describe file('/home/stooj/.config/user-dirs.locale') do
    it { should_not exist }
  end
end
