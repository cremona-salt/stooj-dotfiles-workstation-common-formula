# frozen_string_literal: true

packages = [
  'python-pipenv',
  'python-pip',
  'python-virtualenv',
  'python-virtualenvwrapper'
]

packages.each do |pkg|
  control 'stooj_dotfiles_workstation_common python ' + pkg + ' package' do
    title 'should not be installed'

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
