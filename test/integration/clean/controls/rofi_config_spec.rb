# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common rofi config file' do
  title 'should not exist'

  describe file('/home/stooj/.config/rofi/config') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common rofi config dir' do
  title 'should not exist'

  describe directory('/home/stooj/.config/rofi') do
    it { should_not exist }
  end
end
