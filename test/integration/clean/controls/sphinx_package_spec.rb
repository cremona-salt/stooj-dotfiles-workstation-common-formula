# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common sphinx package' do
  title 'should not be installed'

  describe package('python-sphinx') do
    it { should_not be_installed }
  end
end
