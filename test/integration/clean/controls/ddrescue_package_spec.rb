# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common ddrescue package' do
  title 'should not be installed'

  describe package('ddrescue') do
    it { should_not be_installed }
  end
end
