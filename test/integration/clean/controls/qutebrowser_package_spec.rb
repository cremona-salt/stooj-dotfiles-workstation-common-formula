# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common qutebrowser package' do
  title 'should not be installed'

  describe package('qutebrowser') do
    it { should_not be_installed }
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser launch script' do
  title 'should not exist'

  describe file('/home/stooj/bin/qutebrowser') do
    it { should_not exist }
  end
end
