# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common cronie service' do
  impact 0.5
  title 'should not be running or enabled'

  describe service('cronie') do
    it { should_not be_enabled }
    it { should_not be_running }
  end
end
