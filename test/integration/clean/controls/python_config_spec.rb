# frozen_string_literal: true
#
control 'stooj_dotfiles_workstation_common python virtualenv zsh file' do
  title 'should not exist'

  describe file('/home/stooj/.config/zsh/rc_includes/virtualenv.zsh') do
    it { should_not exist }
  end
end
