# frozen_string_literal: true

packages = [
  'jre-openjdk',
  'jre8-openjdk',
]
packages.each do |pkg|
  control 'stooj_dotfiles_workstation_common ' + pkg + ' package' do
    title 'should not be installed'
  
    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
