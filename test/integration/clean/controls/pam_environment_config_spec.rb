# frozen_string_literal: true
#
control 'stooj_dotfiles_workstation_common pam_environment config file' do
  title 'should be absent'

  describe file('/home/stooj/.pam_environment') do
    it { should_not exist }
  end
end
