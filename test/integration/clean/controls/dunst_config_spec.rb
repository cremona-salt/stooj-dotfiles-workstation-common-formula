# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common dunst config file' do
  title 'should not exist'

  describe file('/home/stooj/.config/dunst/dunstrc') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common dunst config dir' do
  title 'should not exist'

  describe directory('/home/stooj/.config/dunst') do
    it { should_not exist }
  end
end
