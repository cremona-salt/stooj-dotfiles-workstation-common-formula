# frozen_string_literal: true
#
control 'stooj_dotfiles_workstation_common profile config file' do
  title 'should be absent'

  describe file('/home/stooj/.profile') do
    it { should_not exist }
  end
end
