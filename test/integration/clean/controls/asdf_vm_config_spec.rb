# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common asdf_vm env zsh file' do
  title 'should not exist'

  describe file('/home/stooj/.config/zsh/env_includes/asdf_vm.zsh') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common asdf_vm rc zsh file' do
  title 'should not exist'

  describe file('/home/stooj/.config/zsh/rc_includes/asdf_vm.zsh') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common asdf_vm base tool versions file' do
  title 'should not exist'

  describe file('/home/stooj/.tool-versions') do
    it { should_not exist }
  end
end
