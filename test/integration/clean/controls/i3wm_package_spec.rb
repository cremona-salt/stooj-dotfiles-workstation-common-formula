# frozen_string_literal: true

packages = ['i3-gaps', 'i3blocks', 'i3lock', 'i3status']

packages.each do |pkg|
  control 'stooj_dotfiles_workstation_common ' + pkg + ' package' do
    title 'should not be installed'

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end

