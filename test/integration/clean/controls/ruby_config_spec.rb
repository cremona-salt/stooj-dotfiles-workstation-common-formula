# frozen_string_literal: true
#
control 'stooj_dotfiles_workstation_common ruby zsh file' do
  title 'should not exist'

  describe file('/home/stooj/.config/zsh/env_includes/ruby.zsh') do
    it { should_not exist }
  end
end
