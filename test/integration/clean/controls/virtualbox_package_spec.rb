# frozen_string_literal: true

packages = [
    'virtualbox',
    'virtualbox-host-modules',
    'virtualbox-guest-iso',
]
packages.each do |pkg| 
  control 'stooj_dotfiles_workstation_common virtualbox ' + pkg + ' package' do
    title 'should not be installed'

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
