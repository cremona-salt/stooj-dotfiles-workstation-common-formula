# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common w3m package' do
  title 'should not be installed'

  describe package('w3m') do
    it { should_not be_installed }
  end
end
