# frozen_string_literal: true
#
control 'stooj_dotfiles_workstation_common tmate config dir' do
  title 'should be absent'

  describe directory('/home/stooj/.config/tmate') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common tmate config file' do
  title 'should be absent'

  describe file('/home/stooj/.config/tmate/tmate.conf') do
    it { should_not exist }
  end
end
