# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common msmtp config file' do
  title 'should not exist'

  describe file('/home/stooj/.config/msmtp/config') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common msmtp config dir' do
  title 'should not exist'

  describe directory('/home/stooj/.config/msmtp') do
    it { should_not exist }
  end
end
