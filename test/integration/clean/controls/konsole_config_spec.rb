# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common konsole zsh config file' do
  title 'should not exist'

  describe file('/home/stooj/.config/zsh/env_includes/konsole.zsh') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common konsole config file' do
  title 'should not exist'

  describe file('/home/stooj/.config/konsolerc') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common konsole stoos profile file' do
  title 'should not exist'

  describe file('/home/stooj/.local/share/konsole/stoos-profile.profile') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common konsole gruvbox colorscheme file' do
  title 'should not exist'

  describe file('/home/stooj/.local/share/konsole/Gruvbox_dark.colorscheme') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common konsole config dir' do
  title 'should not exist'

  describe directory('/home/stooj/.local/share/konsole') do
    it { should_not exist }
  end
end
