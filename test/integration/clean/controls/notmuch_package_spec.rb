# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common notmuch package' do
  title 'should not be installed'

  describe package('notmuch') do
    it { should_not be_installed }
  end
end
