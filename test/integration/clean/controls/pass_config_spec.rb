# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common pass repo' do
  title 'should not exist'

  describe file('/home/stooj/.kitchen-pass-store') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common pass alias file' do
  title 'should not exist'

  describe file('/home/stooj/.config/zsh/rc_includes/kitchen-pass-alias.zsh') do
    it { should_not exist }
  end
end
