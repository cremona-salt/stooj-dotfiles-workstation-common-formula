# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common config file qutescript repo installed' do
  title 'should not exist'
  describe directory('/home/stooj/.local/lib/python3.8/site-packages/qutescript') do
    it { should_not exist }
  end
end

scripts = ['markdownlink', 'plainlink', 'rstlink']
scripts.each do |script|
  control 'stooj_dotfiles_workstation_common qutebrowser script ' + script do
    title 'should not exist'

    describe file('/home/stooj/.local/share/qutebrowser/userscripts/' + script) do
      it { should_not exist }
    end
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser userscripts dir' do
  title 'should not exist'

  describe directory('/home/stooj/.local/share/qutebrowser/userscripts') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser scripts entrypoint dir' do
  title 'should not exist'

  describe directory('/home/stooj/.local/share/qutebrowser') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser scripts repo' do
  title 'should not exist'

  describe directory('/home/stooj/code/qutebrowser-scripts') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser qutebrowser.conf file' do
  title 'should not exist'

  describe file('/home/stooj/.config/qutebrowser/qutebrowser.conf') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser keys file' do
  title 'should not exist'

  describe file('/home/stooj/.config/qutebrowser/keys.conf') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser config file' do
  title 'should not exist'

  describe file('/home/stooj/.config/qutebrowser/config.py') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser config bookmarks dir' do
  title 'should not exist'

  describe directory('/home/stooj/.config/qutebrowser/bookmarks') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser config dir' do
  title 'should not exist'

  describe directory('/home/stooj/.config/qutebrowser') do
    it { should_not exist }
  end
end

profiles = ['default']
profiles.each do |profile|
  control 'stooj_dotfiles_workstation_common qutebrowser ' + profile + ' launch script' do
  title 'should not exist'
    describe file('/home/stooj/bin/' + profile + '-qutebrowser') do
      it { should_not exist }
    end
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser bookmarks file' do
  title 'should not exist'

  describe file('/home/stooj/.config/qutebrowser/bookmarks/urls') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common qutebrowser quickmarks file' do
  title 'should not exist'

  describe file('/home/stooj/.config/qutebrowser/quickmarks') do
    it { should_not exist }
  end
end
