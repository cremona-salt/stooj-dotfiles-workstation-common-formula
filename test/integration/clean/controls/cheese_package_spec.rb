# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common cheese package' do
  title 'should not be installed'

  describe package('cheese') do
    it { should_not be_installed }
  end
end
