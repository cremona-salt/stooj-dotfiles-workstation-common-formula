# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common notmuch config file' do
  title 'should be absent'

  describe file('/home/stooj/.notmuch-config') do
    it { should_not exist }
  end
end
