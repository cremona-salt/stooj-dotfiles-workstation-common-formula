# frozen_string_literal: true

packages = ['texlive-core', 'texlive-fontsextra' 'texlive-pictures', 'texlive-pstricks', 'texlive-science']
packages.each do |pkg| 
  control 'stooj_dotfiles_workstation_common texlive ' + pkg + ' package' do
    title 'should not be installed'
  
    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
