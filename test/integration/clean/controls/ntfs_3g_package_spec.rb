# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common ntfs_3g package' do
  title 'should not be installed'

  describe package('ntfs-3g') do
    it { should_not be_installed }
  end
end
