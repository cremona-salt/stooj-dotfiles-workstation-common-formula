# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common konsole package' do
  title 'should not be installed'

  describe package('konsole') do
    it { should_not be_installed }
  end
end
