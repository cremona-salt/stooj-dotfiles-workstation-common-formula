# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common code utilities highlight package' do
  title 'should not be installed'

  describe package('highlight') do
    it { should_not be_installed }
  end
end

