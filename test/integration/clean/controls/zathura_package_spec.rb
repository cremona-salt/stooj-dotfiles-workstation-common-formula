# frozen_string_literal: true

packages = ['zathura', 'zathura-djvu', 'zathura-pdf-poppler', 'zathura-ps', 'zathura-cb']
packages.each do |pkg| 
  control 'stooj_dotfiles_workstation_common zathura ' + pkg + ' package' do
    title 'should not be installed'
  
    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
