# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common picom config file' do
  title 'should not exist'

  describe file('/home/stooj/.config/picom/picom.conf') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common picom config dir' do
  title 'should not exist'

  describe directory('/home/stooj/.config/picom') do
    it { should_not exist }
  end
end
