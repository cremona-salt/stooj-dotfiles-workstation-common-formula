# frozen_string_literal: true
#
control 'stooj_dotfiles_workstation_common autostart config file' do
  title 'should be absent'

  describe file('/home/stooj/.config/autostart/gnome-keyring-ssh.desktop') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common autostart config directory' do
  title 'should not exist'
  describe directory('/home/stooj/.config/autostart') do
    it { should_not exist }
  end
end
