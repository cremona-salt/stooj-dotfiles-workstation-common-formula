# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common xss-lock package' do
  title 'should not be installed'

  describe package('xss-lock') do
    it { should_not be_installed }
  end
end
