# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common xdg package' do
  title 'should not be installed'

  describe package('xdg-user-dirs') do
    it { should_not be_installed }
  end
end
