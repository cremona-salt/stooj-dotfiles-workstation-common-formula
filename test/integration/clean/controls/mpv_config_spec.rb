# frozen_string_literal: true
#
control 'stooj_dotfiles_workstation_common mpv config dir' do
  title 'should be absent'

  describe directory('/home/stooj/.config/mpv') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_workstation_common mpv config file' do
  title 'should be absent'

  describe file('/home/stooj/.config/mpv/mpv.conf') do
    it { should_not exist }
  end
end
