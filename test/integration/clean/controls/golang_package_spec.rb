# frozen_string_literal: true

packages = [
  'go',
  'go-tools',
]
packages.each do |pkg|
  control 'stooj_dotfiles_workstation_common golang ' + pkg + ' package' do
    title 'should be installed'

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
