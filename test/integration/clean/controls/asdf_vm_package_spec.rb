# frozen_string_literal: true

control 'stooj_dotfiles_workstation_common asdf_vm repo' do
  title 'should not exist'

  describe directory('/home/stooj/.local/share/asdf') do
    it { should_not exist }
  end
end
